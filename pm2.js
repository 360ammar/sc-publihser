module.exports = {
  apps : [{
    name      : 'PANEL',
    script    : './node_modules/.bin/webpack-dev-server',
    watch: true,
    env: {
      NODE_ENV: 'dev'
    },
    env_production : {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/CMS/publsiher-panel/',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env development'
    }
  }
};