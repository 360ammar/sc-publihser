import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
let user = localStorage.getItem('user');
var decodedToken = user ? jwt_decode(JSON.parse(user).token) : false;
var current_time = Date.now().valueOf() / 1000;



export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        (localStorage.getItem('user') && (decodedToken.exp > current_time))
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)
