/* eslint-disable */
import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import LoginContainer from '../containers/LoginContainer';
import {Home} from '../containers/home';
import ProjectContainer from '../containers/project/index';
import App from '../containers/App';
import jwt_decode from 'jwt-decode';
import PropTypes from "prop-types";
import history from './history'
import NewArtist from '../containers/artist/newArtist';
import { NotFound } from '../containers/404';

Route.propTypes = {
  computedMatch: PropTypes.object,
  path: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  exact: PropTypes.bool,
  strict: PropTypes.bool,
  sensitive: PropTypes.bool,
  component: PropTypes.func,
  render: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  location: PropTypes.object
}


const Routes = () => 
{
	return (
		<React.Fragment>
	<App>
			   <Route exact path="/" component={Home} />
			   <Route path="/project/:id?" component={ProjectContainer}/>
         <Route path="/artist/:id?" component={NewArtist}/>
	</App>
	    <Route path="/login" component={LoginContainer} />
      <Route path="/error-occured" component={NotFound} />
	    </React.Fragment>
	);
};


export default Routes;
