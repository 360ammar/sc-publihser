

function PascalCase(str) {
	return str.match(/[a-z]+/gi)
    .map(function (word) {
      return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase()
    })
.join('')
}

function sec2time(timeInSeconds) {
  var pad = function(num, size) { return ('000' + num).slice(size * -1); },
  time = parseFloat(timeInSeconds).toFixed(3),
  hours = Math.floor(time / 60 / 60),
  minutes = Math.floor(time / 60) % 60,
  seconds = Math.floor(time - minutes * 60);

  console.log(hours, minutes, seconds);
  
  if(hours)  return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
  return pad(minutes, 2) + ':' + pad(seconds, 2);
}

export {PascalCase, sec2time};