import { combineReducers } from 'redux';
import auth from './auth';
import alert from './alert';
import ui from './ui'
import artist from './artist';
import project from './project';
import product from './product';
import track from './track';

export default combineReducers({
  alert,
  auth,
  ui,
  artist,
  project,
  product,
  track
});

