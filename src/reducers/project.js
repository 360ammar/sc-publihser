import { projectConstants } from '../constants/project';

const initialState = { addProjectLoading: false, getProjectLoading: false, getProjectError:false, onNewProjectSuccess: false, getProjectsLoading: false, projects: [], project: {
    project_data : {
        project_name: '',
        code : '',
        description: '',
        project_artist : {
            value : '',
            label: ''
        }
    }
} };

export default function project(state = initialState, action) {
    switch (action.type) {
        case projectConstants.NEW_PROJECT_REQUEST:
            return {
                ...state,
                addProjectLoading: true
            };
        case projectConstants.NEW_PROJECT_SUCCESS:
            return {
                ...state,
                projects: action.project,
                onNewProjectSuccess: true,
                addProjectLoading: false
            };
        case projectConstants.NEW_PROJECT_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case projectConstants.GET_PROJECT_REQUEST:
            return {
                ...state,
                getProjectLoading: true
            };
        case projectConstants.GET_PROJECT_SUCCESS:
            return {
                ...state,
                project: action.project,
                getProjectLoading: false
            };
        case projectConstants.GET_PROJECT_FAILURE:
            return {
                ...state,
                getProjectError: action.error,
                getProjectLoading: false
            };
        case projectConstants.GET_PROJECTS_REQUEST:
            return {
                ...state,
                getProjectsLoading: true
            };
        case projectConstants.GET_PROJECTS_SUCCESS:
            return {
                ...state,
                projects: action.projects
            };
        case projectConstants.GET_PROJECTS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        default:
            return state

    }
}