import { authConstants } from '../constants/auth';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { authenticated: true, user } : { authenticated: false, loggingIn: false };

export default function auth(state = initialState, action) {
    switch (action.type) {
    case authConstants.LOGIN_REQUEST:
        return {
        loggingIn: true,
        user: action.user
        };
    case authConstants.LOGIN_SUCCESS:
        return {
        authenticated: true,
        user: action.user
        };
    case authConstants.LOGIN_FAILURE:
        return {
            authenticated: false
        };
    case authConstants.LOGOUT:
        return {
            authenticated: false
        };
    default:
        return state
    }
}