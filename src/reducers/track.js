import { trackConstants } from '../constants/track.js';

export default function track(state = { uploadingTrack: false, track: {}, selectedTrack:{} }, action) {
    switch (action.type) {
        case trackConstants.CREATE_TRACK_REQUEST:
            return {
                ...state,
                uploadingTrack: true
            };
        case trackConstants.CREATE_TRACK_SUCCESS:
            return {
                ...state,
                uploadingTrack: false,
                track: action.track
            };
        case trackConstants.CREATE_TRACK_FAILURE:
            return {
                uploadingTrack: false,
                errorUploading: true
            };
        case trackConstants.ADD_TRACK_FAILIURE:
            return {
                ...state,
                addingTrack: false,
                
            };
        case trackConstants.ADD_TRACK_SUCCESS:
            return {
                ...state,
                addingTrack: false,
                trackAdd: action.track
            };
        case trackConstants.ADD_TRACK_REQUEST:
            return {
                ...state,
                addingTrack: true
            }; 

        case trackConstants.GET_TRACK_REQUEST:
            return {
                ...state,
                fetchingTrack: true
            };
        case trackConstants.GET_TRACK_SUCCESS:
            return {
                ...state,
                fetchingTrack: false,
                selectedTrack: action.track
            };
        case trackConstants.GET_TRACK_FAILURE:
            return {
                ...state,
                fetchingTrack: false,
                errorTrack: true
            };    

        case trackConstants.CREATE_FORM_TRACK_FAILURE :
            return {
                ...state,
                formUploadErr : true,
                formUploading : false
            }   
            case trackConstants.CREATE_FORM_TRACK_SUCCESS :
            return {
                ...state,
                formUploading : false,
                formUploaded : action.track
            }
            case trackConstants.CREATE_FORM_TRACK_REQUEST :
            return {
                ...state,
                formUploading : true
            }
            
            case trackConstants.INDEX_TRACK_SUCCESS :
            return {
                ...state,
                IndexesUpdateSeccess : action.track
            }
            
        default:
            return state
    }
}