import { productConstants } from '../constants/product';
import artwork from '../containers/project/product/artwork';

const initialState = {
    getProductsLoading: false, products: [],
    createProductRequest: false, newProduct: {},
    fetchingProduct: false,
    basicProductRequest: false,
    genre: {},
    subgenre: {},
    uploadingArtwork: false,
    addMarketing: false,
    marketing : {},
    getMarketing: false
};

export default function product(state = initialState, action) {
    switch (action.type) {

        case productConstants.GET_PRODUCTS_REQUEST:
            return {
                ...state,
                getProductsLoading: true
            };
        case productConstants.GET_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: action.products
            };
        case productConstants.GET_PRODUCTS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case productConstants.GET_PRODUCT_REQUEST:
            return {
                ...state,
                fetchingProduct: true
            };
        case productConstants.GET_PRODUCT_SUCCESS:
            return {
                ...state,
                fetchingProduct: false,
                userAlbum: action.product
            };
        case productConstants.GET_PRODUCT_FAILURE:
            return {
                ...state,
                error: action.error,
                fetchingProduct: false
            };
        case productConstants.CREATE_PRODUCT_REQUEST:
            return {
                ...state,
                createProductRequest: true
            };
        case productConstants.CREATE_PRODUCT_SUCCESS:
            return {
                ...state,
                newProduct: action.product,
                createProductRequest: false
            };
        case productConstants.CREATE_PRODUCT_FAILURE:
            return {
                ...state,
                error: action.error,
                createProductRequest: false
            };

        case productConstants.BASIC_PRODUCT_REQUEST:
            return {
                ...state,
                basicProductRequest: true
            };
        case productConstants.BASIC_PRODUCT_SUCCESS:
            return {
                ...state,
                updatedProduct: action.product,
                basicProductRequest: false
            };
        case productConstants.BASIC_PRODUCT_FAILURE:
            return {
                ...state,
                errorProductBasic: action.error,
                basicProductRequest: false
            };

        case productConstants.GET_GENRE_SUCCESS:
            return {
                ...state,
                genre: action.product
            };

        case productConstants.GET_SUBGENRE_SUCCESS:
            return {
                ...state,
                subgenre: action.product
            };
        case productConstants.ARTWORK_REQUEST:
            return {
                ...state,
                uploadingArtwork : true,
            };

        case productConstants.ARTWORK_SUCCESS:
            return {
                ...state,
                artwork_upload : action.artwork,
                uploadingArtwork : false
            };

        case productConstants.ARTWORK_FAILURE:
            return {
                ...state,
                uploadingArtwork : false,
                errorUploading: true
            };
        case productConstants.ADD_MARKETING_FAILURE:
            return {
                ...state,
                addMarketing : false,
                error: action.error
            };
        case productConstants.ADD_MARKETING_REQUEST:
            return {
                ...state,
                addMarketing: true
            };
        case productConstants.ADD_MARKETING_SUCCESS:
            return {
                ...state,
                addMarketing: false,
                marketing: action.marketing
            };    
        case productConstants.GET_MARKETING_FAILURE:
            return {
                ...state,
                getMarketing : false,
                error: action.error
            };
        case productConstants.GET_MARKETING_REQUEST:
            return {
                ...state,
                getMarketing: true
            };
        case productConstants.GET_MARKETING_SUCCESS:
            return {
                ...state,
                getMarketing: false,
                marketing: action.marketing
            };    

        default:
            return state

    }
}