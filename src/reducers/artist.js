import { artistConstants } from '../constants/artists';

const initialState = { 
    creatingArtist: false ,
    onError: false,
    artist : {},
    artists: [] ,
    uploadingImage: false,
    fetchingArtist : false,
    uploadingSuccess : false,
    errorImageUpload : null
};

export default function artist(state = initialState, action) {
    switch (action.type) {
    case artistConstants.CREATE_ARTIST_REQUEST:
        return {
        ...state,
        creatingArtist: true,
        };
    case artistConstants.CREATE_ARTIST_SUCCESS:
        return {
            ...state,
        creatingArtist: false,
        artistCreated: action.artist
        };
    case artistConstants.CREATE_ARTIST_FALIURE:
        return {
            ...state,
        creatingArtist: false,
        onError: true
        };


        case artistConstants.GET_ARTIST_REQUEST:
        return {
            ...state,
            fetchingArtist: true,
        };
        case artistConstants.GET_ARTIST_SUCCESS:
        return {
            ...state,
            fetchingArtist : false,

        artist: action.artist
        };

    case artistConstants.GET_ARTIST_FALIURE:
        return {
            ...state,
            
            fetchingArtist : false,
        };

    case artistConstants.GET_ARTISTS_REQUEST:
        return {
            ...state,
        };
    case artistConstants.GET_ARTISTS_SUCCESS:
        return {
            ...state,
            artists: action.artists
        };
    case artistConstants.GET_ARTISTS_FAILURE:
        return {
            ...state,
            error: action.error
        };

        case artistConstants.CREATE_ARTIST_IMAGE_REQUEST:
        return {
            ...state,
            uploadingImage : true,
            uploadingSuccess : false,
        };
    case artistConstants.CREATE_ARTIST_IMAGE_SUCCESS:
        return {
            ...state,
            uploadingImage: false,
            uploadingSuccess : true,
            artists: action.artists
        };
    case artistConstants.CREATE_ARTIST_IMAGE_FALIURE:
        return {
            ...state,
            uploadingImage: false,
            uploadingSuccess : false,
            errorImageUpload: action.error
        };   
    default:
        return state
    }
}