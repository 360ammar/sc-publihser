import { uiConstants } from '../constants/ui.js';

export default function ui(state = {isNewProduct: false}, action) {
    switch (action.type) {
    case uiConstants.NEW_PRODUCT:
        return {
        isNewProduct: action.value
        };
    default:
        return state
    }
}