import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Router, Switch } from 'react-router-dom';
import configureStore from './store';
import History from './helpers/history';
import Routes from './helpers/routes';
import './assets/styles/main.css';
import './styles/base.sass';

const store = configureStore();


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Router history={History}>
          <Switch>
            <Routes />
          </Switch>
      </Router>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);
