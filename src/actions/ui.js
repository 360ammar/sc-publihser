import { uiConstants } from '../constants/ui.js';

export const ui = {
    onNewProduct
};

function onNewProduct(value) {
    return { type: uiConstants.NEW_PRODUCT , value };
}
