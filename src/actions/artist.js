/* Artist Actions */

import { artistConstants } from '../constants/artists';
import axios from 'axios';

export const artist = {
    addArtist, 
    getArtist,
    getArtists,
    addArtistImage
}; 
let user = localStorage.getItem('user');

function addArtist(data) {
    return dispatch => {
        dispatch(request())

        axios.post(`https://api.shiacloud.net/private/publisher/artist/add`, data,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
       .then((result) => {
           return dispatch(success(result.data));
         }).catch(err => {
           return dispatch(failure(err));
       });
    }
       function request() { return { type: artistConstants.CREATE_ARTIST_REQUEST } }
       function success(artist) { return { type: artistConstants.CREATE_ARTIST_SUCCESS, artist } }
       function failure(error) { return { type: artistConstants.CREATE_ARTIST_FALIURE, error } }
}

function addArtistImage(data) {
    return dispatch => {
        dispatch(request())
        axios.post(`https://api.shiacloud.net/newartist`, data,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token, 'Content-Type': 'multipart/form-data' }
    })
       .then((result) => {
           return dispatch(success(result.data));
         }).catch(err => {
           return dispatch(failure(err));
       });
    }
       function request(artist) { return { type: artistConstants.CREATE_ARTIST_IMAGE_REQUEST, } }
       function success(artist) { return { type: artistConstants.CREATE_ARTIST_IMAGE_SUCCESS, artist } }
       function failure(error) { return { type: artistConstants.CREATE_ARTIST_IMAGE_FALIURE, error } }
}

function getArtist(id) {
    return dispatch => {
        dispatch(request())
        axios.post(`https://api.shiacloud.net/private/publisher/artist/get`, {id},
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
       .then((result) => {
           return dispatch(success(result.data));
         }).catch(err => {
           return dispatch(failure(err));
       });
    }
       function request() { return { type: artistConstants.GET_ARTIST_REQUEST } }
       function success(artist) { return { type: artistConstants.GET_ARTIST_SUCCESS, artist } }
       function failure(error) { return { type: artistConstants.GET_ARTIST_FALIURE, error } }
}

function getArtists(publisherID, name) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/artist/all`, {publisherID, name},
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                result => {
                    return dispatch(success(result.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: artistConstants.GET_ARTISTS_REQUEST } }
    function success(artists) { return { type: artistConstants.GET_ARTISTS_SUCCESS, artists } }
    function failure(error) { return {  type: artistConstants.GET_ARTISTS_FALIURE, error } }
}