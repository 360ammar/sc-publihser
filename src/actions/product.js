import { productConstants } from '../constants/product';
import axios from 'axios';
export const product = {
    getProducts,
    createProduct,
    getProduct,
    basicProduct,
    getGenre,
    getSubGenre,
    addArtwork,
    addMarketing,
    getMarketing
};

let user = localStorage.getItem('user');

function getProduct(id) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/product/get`,{id, publisherId: JSON.parse(user).publisher.id},
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.GET_PRODUCT_REQUEST } }
    function success(product) { return { type: productConstants.GET_PRODUCT_SUCCESS, product } }
    function failure(error) { return {  type: productConstants.GET_PRODUCT_FAILURE, error } }
}

function getProducts() {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/public/publisher/product/all`,{publisherID : JSON.parse(user).publisher.id},
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.GET_PRODUCTS_REQUEST } }
    function success(products) { return { type: productConstants.GET_PRODUCTS_SUCCESS, products } }
    function failure(error) { return {  type: productConstants.GET_PRODUCTS_FAILURE, error } }
}

function createProduct(data) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/product/create`,data,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.CREATE_PRODUCT_REQUEST } }
    function success(product) { return { type: productConstants.CREATE_PRODUCT_SUCCESS, product } }
    function failure(error) { return {  type: productConstants.CREATE_PRODUCT_FAILURE, error } }
}

function addArtwork(data){
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/addartwork`,data,
        { headers: { 
        Authorization: "Bearer " + JSON.parse(user).token, 
        'content-type': 'multipart/form-data'
    } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.ARTWORK_REQUEST } }
    function success(artwork) { return { type: productConstants.ARTWORK_SUCCESS, artwork } }
    function failure(error) { return {  type: productConstants.ARTWORK_FALIURE, error } }
}


function basicProduct(data) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/product/basic`,data,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.BASIC_PRODUCT_REQUEST } }
    function success(product) { return { type: productConstants.BASIC_PRODUCT_SUCCESS, product } }
    function failure(error) { return {  type: productConstants.BASIC_PRODUCT_FAILURE, error } }
}

function getGenre() {
    return dispatch => {
        axios.get(`https://api.shiacloud.net/private/genre/get`,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data.genre));
                },
                error => {
                    console.log(error);
                    return error;
           
                }
            );
    };

    function success(product) { return { type: productConstants.GET_GENRE_SUCCESS, product } }
}

function getSubGenre() {
    return dispatch => {
        axios.get(`https://api.shiacloud.net/private/subgenre/get`,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data.subgenre));
                },
                error => {
                    console.log(error);
                    return error;
           
                }
            );
    };

    function success(product) { return { type: productConstants.GET_SUBGENRE_SUCCESS, product } }
}

function addMarketing(marketing, productId) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/product/marketing/add`, { marketing, productId },
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                marketing => {
                    return dispatch(success(marketing.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.ADD_MARKETING_REQUEST  } }
    function success(marketing) { return { type: productConstants.ADD_MARKETING_SUCCESS, marketing } }
    function failure(error) { return {  type: productConstants.ADD_MARKETING_FAILURE, error } }
}

function getMarketing(productId) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/product/marketing/get`, { productId },
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                marketing => {
                    return dispatch(success(marketing.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: productConstants.GET_MARKETING_REQUEST  } }
    function success(marketing) { return { type: productConstants.GET_MARKETING_SUCCESS, marketing } }
    function failure(error) { return {  type: productConstants.GET_MARKETING_FAILURE, error } }
}