import { trackConstants } from '../constants/track';
import axios from 'axios';
export const track = {
    uploadTrack,
    addTrack,
    getTrack,
    uploadFormTrack,
    updateIndex
};

let user = localStorage.getItem('user');

function uploadTrack(trackFile) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/uploadtrack`,trackFile,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: trackConstants.CREATE_TRACK_REQUEST } }
    function success(track) { return { type: trackConstants.CREATE_TRACK_SUCCESS, track } }
    function failure(error) { return {  type: trackConstants.CREATE_TRACK_FAILURE, error } }
}

function uploadFormTrack(trackFile) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/uploadtrack`,trackFile,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                products => {
                    return dispatch(success(products.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: trackConstants.CREATE_FORM_TRACK_REQUEST } }
    function success(track) { return { type: trackConstants.CREATE_FORM_TRACK_SUCCESS, track } }
    function failure(error) { return {  type: trackConstants.CREATE_FORM_TRACK_FAILURE, error } }
}


function addTrack(track) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/track/add`,track,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                tracks => {
                    return dispatch(success(tracks.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: trackConstants.ADD_TRACK_REQUEST } }
    function success(track) { return { type: trackConstants.ADD_TRACK_SUCCESS, track } }
    function failure(error) { return {  type: trackConstants.ADD_TRACK_FAILIURE, error } }
}

function getTrack(id) {
    return dispatch => {
        dispatch(request());
        axios.post(`https://api.shiacloud.net/private/publisher/track/get`,{'trackId' : id},
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                tracks => {
                    return dispatch(success(tracks.data));
                },
                error => {
                    console.log(error);
                    return dispatch(failure(error));
           
                }
            );
    };

    function request() { return { type: trackConstants.GET_TRACK_REQUEST } }
    function success(track) { return { type: trackConstants.GET_TRACK_SUCCESS, track } }
    function failure(error) { return {  type: trackConstants.GET_TRACK_FAILURE, error } }
}

function updateIndex(indexes) {
    return dispatch => {
        axios.post(`https://api.shiacloud.net/private/publisher/track/index`, {indexes} ,
        { headers: { Authorization: "Bearer " + JSON.parse(user).token } })
            .then(
                track => {
                    return dispatch(success(track.data))
                }
            )

            function success(track) { return { type: trackConstants.INDEX_TRACK_SUCCESS, track } }
        
    }
}