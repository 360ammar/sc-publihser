import { authConstants } from '../constants/auth';
import { alert } from './alert';
import History from '../helpers/history';
import axios from 'axios';
export const auth = {
    login,
    logout
};

function login(email, password) {
    return dispatch => {
        dispatch(request( email));
        axios.post(`https://api.shiacloud.net/public/publisher/login`, {email, password})
        .then(({data}) => {
            localStorage.setItem('user', JSON.stringify(data));
            window.location.replace('/');
            return dispatch(success(data));
          }).catch(err => {
            return dispatch(failure(err));
        });
    }

    function request(user) { return { type: authConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: authConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: authConstants.LOGIN_FAILURE, error } }
}

function logout() {
    localStorage.removeItem('user');
    window.location.replace('/login');
    return { type: authConstants.LOGOUT };
}
