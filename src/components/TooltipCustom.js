import React from 'react';
import { Tooltip } from 'reactstrap';
import {FaInfoCircle} from 'react-icons/fa'


export default class TooltipCustom extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false,
      id: Math.random().toString(36).replace(/[^a-z]+/g, '')
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render() {
    return (
      <span >
        <a id={this.state.id}>{this.props.children || <FaInfoCircle /> }</a>
        <Tooltip placementPrefix="" placement={this.props.placement || 'top'}  isOpen={this.state.tooltipOpen} target={this.state.id} toggle={this.toggle} autohide={false} delay={10}>
          {this.props.text}
        </Tooltip>
      </span>
    );
  }
}