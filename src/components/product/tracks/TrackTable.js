import React from 'react';
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd';
import { TrackItem } from './TrackItem'
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
  arrayMove,
} from 'react-sortable-hoc';
import {Badge} from 'reactstrap'
import { PulseLoader } from 'react-spinners';
import { orderBy, uniq ,find } from 'lodash'
// This can be any component you want

const SortableItem = SortableElement((props) => <TrackItem  indexUpdate={props.indexUpdate} itemClicked={props.itemClicked} activeItem={props.activeItem} track={props.value.Track ? props.value.Track : props.value } value={props.value} id={props.id} /> );

const SortableList = SortableContainer((props) => {
  return (
  	<tbody>
    {props.items.length > 0 && props.items.map((value, index) => (
        <SortableItem  indexUpdate={props.indexUpdate} key={`item-${index}`} activeItem={props.activeItem} itemClicked={props.itemClicked.bind(this)} index={index}  id={index} value={value} />
			))}
			{
		props.uploading		&& <tr style={{background: '#f1f1f1'}} className={`TrackRow TrackRow-hoverArrow`}>
				<td className="TrackRow-id-cell">{''}</td>
			<td className="DragArrowsRow TrackRow-with-border">
					
			</td>
			{/* <td className="TrackRow-with-border">
					<div className="TrackRowCheckbox checkbox">
							<input id="762212419327319254" className="checkbox-input" value="on" type="checkbox" />
									<label className="" for="762212419327319254"></label>
							</div>
					</td> */}
					<td className="TrackRow-with-border">
						<PulseLoader loading size={10} color={'#6c757d'} />
					</td>
					<td className="TrackRow-with-border">
							
									<Badge>Uploading</Badge>
												</td>
					<td className="TrackRow-with-border"></td>
			 </tr>
			}
   	</tbody>
  );
});


class TrackTable extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			items: [],
			indexes : []
		}
		this.indexUpdate = this.indexUpdate.bind(this)
	}

	indexUpdate(item,index) {
			const { indexes, items } = this.state;
			let indexUpdate = indexes;
			indexUpdate.push({ id : item , index })
			this.setState({ indexes : indexUpdate })
			
	}


	componentDidMount(){
		this.setState({items : orderBy(this.props.tracks , ['index'] , ['asc']) })
	}

	componentDidUpdate(prevProps,prevState){
		const { indexes , items } = this.state
		let tracksCheck = items.filter(val => val.Track ? val.Track.index === 0 : 0);
		let uploadedtracksCheck = items.filter(val => val.index === 0);
		if(indexes.length > 0 && (tracksCheck.length === indexes.length) || uploadedtracksCheck.length > 0 && (uploadedtracksCheck.length === indexes.length) ){
				 	this.props.updateIndex(indexes);
					this.setState({ indexes : [] })
		}

		
		if((this.props.uploadedTrack !== prevProps.uploadedTrack) && ( !this.props.uploading && prevProps.uploading )){
			let items = Array.from(this.state.items)
			items.push(this.props.uploadedTrack) 
			this.setState({ items: orderBy(items , ['index'] , ['asc'])  })
		}
	}

	onSortEnd({oldIndex, newIndex}) {
    	const {items} = this.state;

	    this.setState({
	      items: arrayMove(items, oldIndex, newIndex),
	    });
	 
	};



	
  render() {
  	
  	const {items} = this.state;
    return (
		<div className="TrackBuilder-tracks-table">
				<table className="TracksTable">
					<thead><tr className="TracksTable-head-row">
							<th className="TracksTable-track-number-column">#</th>
							<th className="TracksTable-drag-arrows-column"></th>
							{/* <th className="TracksTable-checkbox-column">
							<div className="TracksTable-checkbox checkbox">
							<input id="527141211329869551" className="checkbox-input" value="on" type="checkbox" />
							<label className="" for="527141211329869551"></label></div>
							</th> */}
							<th className="TracksTable-name-column">Track</th>
							<th>Audio</th>
							<th></th>
						</tr>
					</thead>
					
					<SortableList items={items} indexUpdate={this.indexUpdate} lockAxis={'y'} uploading={this.props.uploading} activeItem={this.props.activeItem} itemClicked={this.props.itemClicked} helperClass={'TrackRow-dragging'} onSortEnd={this.onSortEnd.bind(this)} useDragHandle={true} />
					
				
				</table>
		</div>	
    );		
  }
}


export default TrackTable;