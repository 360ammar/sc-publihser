import React from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, Label, Input, Button} from 'reactstrap';
import Select from 'react-select';
import TooltipCustom from '../../TooltipCustom';
import data from '../../../data'
import {artist} from '../../../actions/artist'
import {track} from '../../../actions/track'
import { get, find } from 'lodash'
import { PulseLoader } from 'react-spinners'


class TrackForm extends React.Component {
	constructor(props){
		super(props);
    this.state = { inputValue: '',
    primary_artists : '',
    featured_artists : '',
    remixers: '',
    producers: '',
    writers: '',
    track_name : '',
    isrc_code: '',
    lyrics: '',
		cline: '',
		trackId: 0,
		track_language: {},
		master_rights: {},
		recording_country: {},
		

  };
	}
	

  handleValueChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}
  componentDidMount(){
		this.props.getArtists(this.props.auth.user.publisher.id, "");
		
		const {     track_name,
    isrc,
    lyrics,
		line,
			track_language,
			recording_country,
			id,
			master_rights,
			primaryartists,
			featuredartists,
			producer_artists,
			remixer_artists,
			writers_artists
	} = this.props.track;

		this.setState({
			track_name,
			isrc_code : isrc,
			lyrics,
			cline : line,
			trackId: id,
			track_language : find(data.languages, ['label' , track_language]),
			recording_country:  find(data.countries, ['label' , recording_country]),
			master_rights: find( data.masterRights , ['label' , master_rights] ),
			primary_artists : primaryartists.length > 0 && primaryartists.map((artist) => {
													return { 'label': artist.Artist.name, 'value': artist.Artist.id }
												}),

			featured_artists : featuredartists.length > 0 && featuredartists.map((artist) => {
				return { 'label': artist.Artist.name, 'value': artist.Artist.id }
			}),


			producers : producer_artists.length > 0 && producer_artists.map((artist) => {
				return { 'label': artist.Artist.name, 'value': artist.Artist.id }
			}),


			remixers : remixer_artists.length > 0 && remixer_artists.map((artist) => {
				return { 'label': artist.Artist.name, 'value': artist.Artist.id }
			}),

			
			writers : writers_artists.length > 0 && writers_artists.map((artist) => {
				return { 'label': artist.Artist.name, 'value': artist.Artist.id }
			}),

			
		})



  }
  
	handlePrimaryArtistsChange(selected_artist) {
		this.setState({ primary_artists: selected_artist })
	}

	handleFeaturedArtistsChange(selected_artist) {
		this.setState({ featured_artists: selected_artist })
	}

	handleProducerArtistsChange(selected_artist) {
		this.setState({ producers: selected_artist })
	}
	handleRemixerArtistsChange(selected_artist) {
		this.setState({ remixers: selected_artist })
	}
  handleWriterArtistChange(selected_artist) {
    this.setState({writers: selected_artist})
  }
  handleLyricsLanguageChange(selected_artist) {
    this.setState({lyrics_language: selected_artist})
  }
  handleOwnershipChange(selected_artist) {
    this.setState({master_rights: selected_artist})
	}
	handleCountryChange(selected_artist) {
		this.setState({ recording_country : selected_artist })
	}

  handleInputChange (newValue){
    const inputValue = newValue.replace(/\W/g, '');
    this.setState({ inputValue });
    return inputValue;
};
handleTrackFile(e){
  this.setState({trackFile : e.target.files[0], track_name: e.target.files[0].name})
  let formData = new FormData();
	formData.append('track-file', e.target.files[0]);
	formData.append('trackid', this.props.track.id )
  this.props.uploadFormTrack(formData);
}

submitHandle (e) {

	const {
		primary_artists,
    featured_artists,
    remixers,
    producers,
    writers,
    track_name,
    isrc_code,
    lyrics,
		cline,
		trackId,
		track_language,
		master_rights,
		recording_country
	 } = this.state;

	 this.props.addTrack({		primary_artists,
    featured_artists,
    remixers,
    producers,
    writers,
    track_name,
    isrc : isrc_code,
    lyrics,
		line: cline,
		trackId,
		track_language : track_language.label ? track_language.label  : '',
		master_rights : master_rights.label ? master_rights.label : '',
		recording_country: recording_country.label ? recording_country.label : ''
 	 }
	 )




}

	render() {

    const artist_data = get(this.props.artist, 'artists.artistPublisher.docs', [{ Artist : { value: '', id : ''}}]);
    console.log(this.props)
    const {  
      
      primary_artists,
      featured_artists,
      remixers,
			producers,
      writers,
			track_name,
			isrc_code,
			lyrics,
			cline,
				track_language,
				recording_country,
				
				master_rights
		} = this.state;
    console.log(this.audio)

		return (
			<div className="TrackEditForm">
				<div className="TrackEditForm-header-container">
					<h4 className="TrackEditForm-header">{`Track ${this.props.trackIndex} - Details`}</h4>
				</div>
				<div className="TrackEditForm-form">

				<FormGroup required>
		    			<Label for="audio-file">Audio File * <TooltipCustom text="The Orchard's standard digital audio specification is WAV (.wav), PCM codec, 16-bit or 24-bit, 44.1kHz, Stereo. If this track is Mastered for iTunes (MFiT), please inform your client manager directly." /></Label>
		    	{ this.props.uploading
						?
					<div className="content-loader" >
						<PulseLoader  loading  color={'#9b9b9b'} size={15} margin={'10px'} />
					</div>
					:	<React.Fragment><h4 className="mt-0" >{this.props.track.track_name}</h4>
						<audio style={{ 'width': '65%' , 'verticalAlign': 'text-top' }} 
						ref={c => this.audio = c}
						src={`http://api.shiacloud.net/${this.props.uploadedTrack ? this.props.uploadedTrack.track_upload.mediaFile : this.props.track.mediaFile}`}  controls />

						<div className="TrackEditForm-audio-file float-right btn btn-default">
						<Input type="file" id="audio-file" className=" audio-change" accept="audio/mpeg,audio/wav" name="audio-file" onChange={this.handleTrackFile.bind(this)} /></div></React.Fragment>	}	</FormGroup>
		    		

			<FormGroup required>
		    			<Label for="track-name">Track Name * <TooltipCustom text="The name of this track / composition, as you want it to appear in stores." /></Label>
		    			<Input type="text" value={track_name} onChange={this.handleValueChange.bind(this)} id="track-name" name="track_name" />
		    		</FormGroup>
		    		<FormGroup>
		    			<Label for="name-displayed">Track Name Displayed As <TooltipCustom text="This is how the track name and version fields will be combined and displayed at stores like iTunes and Apple Music." /></Label>
		    			<Input type="text" readOnly value={track_name} className="DisplayedAs-input" id="name-displayed" name="name-displayed"  />
		    		</FormGroup>
		    		 			<FormGroup required>
		    			<Label for="meta-language">Lyrics Language * <TooltipCustom text="This is the language used in the performance of the track. For instrumentals, please select 'Instrumental' as your Lyrics Language." /></Label>
		    			
		    		<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						options={data.languages}
						onChange={this.handleLyricsLanguageChange.bind(this)}
						value={track_language}
		              id="meta-language"
					  name="meta-language"
					/>
			
		    		</FormGroup>	
		    		<FormGroup>
		    			<Label for="isrc-code">ISRC * <TooltipCustom text="An International Standard Recording Code, or ISRC, is a 12-character alphanumeric code that identifies a unique sound recording or music video. If this track has already been distributed by you or another party, then you must use the original ISRC. You cannot use the same ISRC for an audio track and its music video version. ISRC misuse can result in lost revenue." /></Label>
		    			<Input type="text" value={isrc_code} onChange={this.handleValueChange.bind(this)} id="isrc-code" name="isrc_code" />
		    		</FormGroup>
		    		<FormGroup required>
		    			<Label for="cline">(c)Line * <TooltipCustom text="The phonographic rights line, also referred to as the sound recording copyright symbol, (P) Line, or ℗ Notice, provides notice of copyright in a sound recording. Enter the first year this sound recording was released, plus the entity that originally owned the master sound recording. For example: '1990 Original Owner's Name' or '2017 My Label's Name'." /></Label>
		    			<Input type="text" value={cline}  onChange={this.handleValueChange.bind(this)} id="cline" name="cline" />
		    		</FormGroup>
		    			<FormGroup>
		    			<Label for="track-lyrics">Track Lyrics <TooltipCustom text="Please refer to Apple Lyrics Guidelines for all formatting requirements" /></Label>
		    			<Input type="textarea" value={lyrics}  onChange={this.handleValueChange.bind(this)} id="track-lyrics" name="lyrics" />
		    		</FormGroup>


<h4 className="TrackEditForm-artists-header">Artists &amp; Contributors</h4>


<FormGroup required>
		    			<Label for="primary-artist">Primary Artist(s) * <TooltipCustom text="The names of the main artists for this track." /></Label>
		    			
					<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={primary_artists}
						onChange={this.handlePrimaryArtistsChange.bind(this)}
						onInputChange={this.handleInputChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
							return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="primary-artist"
						/> 
			
		    		</FormGroup>	

		    		<FormGroup>
		    			<Label for="featured-artist">Featured Artist(s) <TooltipCustom text="The names of any artists featured on this track." /></Label>
		    			
							<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={featured_artists}
						onChange={this.handleFeaturedArtistsChange.bind(this)}
						onInputChange={this.handleInputChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
							return { 'label': artist.Artist.name, 'value': artist.Artist.id.toString() }
						})}


					  name="featured_artist"
							/> 
			
		    		</FormGroup>
		    			<FormGroup>
		    			<Label for="remixers">Remixer(s) <TooltipCustom text="The names of any remixers featured on this track." /></Label>
		    			
		   	<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={remixers}
						onChange={this.handleRemixerArtistsChange.bind(this)}
						onInputChange={this.handleInputChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
							 return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="remixers"
					/> 
				</FormGroup>
		    			<FormGroup>
		    			<Label for="producers">Producer(s) <TooltipCustom text="The names of any producers featured on this track." /></Label>
		    			
							<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={producers}
						onChange={this.handleProducerArtistsChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
						  return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="producers"
						/> 
			
		    		</FormGroup>
		    				<FormGroup>
		    			<Label for="writers">Writer(s) <TooltipCustom text="Enter the full birth names of all songwriters for this track. Accurate songwriter information is important to ensure that this track can be licensed in a timely manner. Failure to provide accurate songwriter information can result in lost revenue due to a track's inability to be licensed." /></Label>
		    			
		    		<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
            isSearchable
            value={writers}
            isMulti
            onChange={this.handleWriterArtistChange.bind(this)}
            options={artist_data && artist_data.map((artist) => {
						  return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="writers"
					/>
			
		    		</FormGroup>

		    		<div className="RightsInfoFields">
		    		<h4 className="RightsInfoFields-header">Master Rights</h4>


		    			<FormGroup required>
		    			<Label for="marter-rights">Ownership for this Sound Recording * <TooltipCustom text="The owner of the master copyright is entitled to claim royalties generated by various uses of the “master” sound recording. Depending on the country, royalties can be generated from “public performances” of a sound recording, such as when played at a bar or nightclub, or when broadcast on television or radio. The uses of a sound recording in user-generated content (UGC), such as YouTube videos, can also generate royalties. The master rights usually belong to the entity that financed a song’s recording, which is typically a record label." /></Label>
		    			<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
						isSearchable={true}
						options={data.masterRights}
						onChange={this.handleOwnershipChange.bind(this)}
						value={master_rights}
					  name="marter-rights"
					/>
		    		</FormGroup>
		    		<FormGroup>
		    			<Label for="name-displayed">Country of Recording <TooltipCustom text="Choose the country where the majority of the recording sessions for this track took place." /></Label>
		    		
		    		<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						onChange={this.handleCountryChange.bind(this)}	
						options={data.countries}
		              noOptionsMessage={ 'Select ...' }
						value={recording_country}
					  name="country"
					/>

		    			</FormGroup>
		    	
		    	

		    		</div>



				</div>

				<div className="TrackEditForm-change-track-buttons">
					{/* <button id="previous-track" disabled="" type="button" className="btn btn-default">
						<span className="TrackEditForm-arrow-symbol"> ← </span><span>Edit previous track</span>
					</button> */}
					<button onClick={this.submitHandle.bind(this)} type="button" className="btn btn-primary float-right">
						<span>Save</span><span className="TrackEditForm-arrow-symbol">→</span>
					</button>
				</div>
			</div>
			);
	}
}

function mapDispatchToProps(dispatch) {
  return({
  getArtists: (publisher, name) => {dispatch(artist.getArtists(publisher, name))},
	uploadTrack: (trackFile) => {dispatch(track.uploadTrack(trackFile))},
	addTrack: (trackInfo) => { dispatch(track.addTrack(trackInfo)) }

  })
}

function mapStateToProps(state) {
    return { 
    artist: state.artist,
    auth: state.auth,  
    }
}




export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);