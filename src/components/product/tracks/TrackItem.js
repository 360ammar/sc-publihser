import React from 'react'
import { MdSwapVert } from "react-icons/md";
import { FaPlay, FaPause } from 'react-icons/fa'
import { SortableHandle } from 'react-sortable-hoc';
import { Link } from 'react-router'
import { sec2time } from '../../../helpers/utils'
const DragHandle = SortableHandle(() => <MdSwapVert />); 

export class TrackItem extends React.Component {

    componentDidMount(){
    if(this.props.track.index === 0){
        this.props.indexUpdate(this.props.track.id, Number(this.props.id)+1)
    }
    }

    render(){
        const { props } = this;
   return( <tr onClick={props.itemClicked.bind(this,{item : props.track.id, index: parseInt(props.id)+1 })} className={`TrackRow TrackRow-hoverArrow ${props.activeItem === props.track.id && 'TrackRow-selected-track TrackRow-triangle'}`}>
    <td className="TrackRow-id-cell">{parseInt(props.id)+1}</td>
  <td className="DragArrowsRow TrackRow-with-border">
      <DragHandle />
  </td>
  {/* <td className="TrackRow-with-border">
      <div className="TrackRowCheckbox checkbox">
          <input id="762212419327319254" className="checkbox-input" value="on" type="checkbox" />
              <label className="" for="762212419327319254"></label>
          </div>
      </td> */}
      <td className="TrackRow-with-border">
          <div>{!!props.track.track_name ? props.track.track_name : 'Unnamed Track'}</div>
      </td>
      <td className="TrackRow-with-border">
      { true ? <span>
          
          <audio id="player" src={`https://api.shiacloud.net/${props.track.mediaFile}`} /> {sec2time(props.track.duration)}</span>
        :  <span className="TrackRow-asset-failed-label">
              <span>Upload Error</span>
    </span> }
      </td>
      <td className="TrackRow-with-border"></td>
   </tr> );
    }
}