

import React from 'react';
export class DigitalProductOverviewPublishingObligation extends React.Component {
	
  render() {

  

    return (
    	<table className="DigitalProductOverviewTracks-table">
	    	<thead>
		    	<tr className="DigitalProductOverviewTracks-head-row">
		    		<th className="DigitalProductOverviewTracks-column-title"></th>
		    		<th className="DigitalProductOverviewTracks-column-title">Track Name</th>
		    		<th className="DigitalProductOverviewTracks-column-title">US Publishing Obligation</th>
		    		<th className="DigitalProductOverviewTracks-column-title">Publishers</th>
		    	</tr>
	    	</thead>
	    	<tbody className="DigitalProductOverviewTracks-track-row">
	    		<tr>
	    			<td className="DigitalProductOverviewTracks-track-number">1</td>
	    			<td className="DigitalProductOverviewTracks-track-name">Ae Shaheed e Karbala Tujh Par Salam</td>
	    			<td className="DigitalProductOverviewTracks-us-pub-obligation">
	    				<span className="DigitalProductOverviewTracks-us-pub-obligation-label">100% controlled or administered by my label</span>
	    			</td>
	    			<td className="DigitalProductOverviewTracks-publishers">Ajareresalat.com</td>
	    		</tr>
	    	</tbody>
    	</table>
    );
  }
}

