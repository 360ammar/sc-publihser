import React from 'react';
import { sec2time } from '../../../helpers/utils'
export class DigitalProductOverviewTracks extends React.Component {
	
  render() {

  

    return (
    	<table className="DigitalProductOverviewTracks-table">
	    	<thead>
		    	<tr className="DigitalProductOverviewTracks-head-row">
		    		<th className="DigitalProductOverviewTracks-column-title"></th>
		    		<th className="DigitalProductOverviewTracks-column-title">Track Name</th>
		    		<th className="DigitalProductOverviewTracks-column-title">Primary Artist(s)</th>
		    		<th className="DigitalProductOverviewTracks-column-title">ISRC</th>
		    		<th className="DigitalProductOverviewTracks-column-title">Length</th>
		    	</tr>
	    	</thead>
	    	<tbody>
						{ this.props.tracks.map((track, index) => {
								return <tr className="DigitalProductOverviewTracks-track-row">
								<td className="DigitalProductOverviewTracks-track-number">{index+1}</td>
								<td className="DigitalProductOverviewTracks-track-item"> {track.track_name}</td>
								<td className="DigitalProductOverviewTracks-track-item">
									<span className="DigitalProductOverviewTracks-track-item-performers">{track.artist_name}</span>
								</td>
								<td className="DigitalProductOverviewTracks-track-item">{track.isrc}</td>
								<td className="DigitalProductOverviewTracks-track-item">
									<span className="DigitalProductOverviewTracks-track-item-audio-controls">
										<p className="DigitalProductOverviewTracks-track-item-audio-duration">{sec2time(track.duration)}</p>
									</span>
								</td>
							</tr>
					})
	    		 }
	    	</tbody>
    	</table>
    );
  }
}

