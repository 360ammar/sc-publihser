import React from 'react';
import {PascalCase} from '../../../helpers/utils';

export const DigitalProductOverviewItem = ({ item }) => {
     const {extraOverlayClassNames, fieldErrors, isRequired ,title, value} = item;
  return (
    
      <div className="DigitalProductOverviewItem" id={`item${PascalCase(title)}`}>
        <label className="DigitalProductOverviewItem-title">{title}{' '}{isRequired ? '*' : null}</label>
        <p className="DigitalProductOverviewItem-value">{value ? value : '-'}</p></div>

  );
};


export default DigitalProductOverviewItem;
