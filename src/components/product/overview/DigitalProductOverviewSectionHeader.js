import React from 'react';
import {Link} from 'react-router-dom';
export class DigitalProductOverviewSectionHeader extends React.Component {
	
  render() {

  	const {title, isEditable, isDanger, linkRef} = this.props;

    return (
    	<div className="DigitalProductOverviewSectionHeader">
    	<h3 className={`DigitalProductOverviewSectionHeader-title ${isDanger ? 'DigitalProductOverviewSectionHeader-title-danger' : null}`}>{title}</h3>
    	   {isEditable? <Link to={linkRef} className="DigitalProductOverviewSectionHeader-button btn btn-default pull-right"
    		id={`edit${title}Button`} ><i className="ion-edit"></i><span className="DigitalProductOverviewSectionHeader-button-text">Edit</span></Link> : null}
    	</div>
    );
  }
}

