import React from 'react'
import { FormGroup, Input } from 'reactstrap';
import Select from 'react-select';
import TrippleCheckmark  from '../TrippleCheckmark'

const publishingOb = [{
	'label': '100% controlled or administered by my label',
	'value': '100-controlled'
},
{
	'label': 'Less than 100% controlled (at least partially administered by 3rd party)',
	'value': 'lt-100-controlled'
},
{
	'label': 'Public Domain in the US, or non-music',
	'value': 'public-domain'
}]

class PublishingTable extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            pubOb : '',
            publisher : ''
        };
    }

    componentDidUpdate(prevProps){
        if(prevProps.publisher !== this.props.publisher){
            this.setState({publisher: this.props.publisher, pubOb: this.props.pubOb})
        }
    }

    applyOver(index) {
		this.setState({ showApplyAll: index });
	}

	applyOut() {
		this.setState({ showApplyAll: undefined });
	}

    handleSelectChange (option) {
        this.setState({pubOb : option})
    }

    handleValueChange (e) {
        this.setState({publisher : e.target.value})
    }

    render() {
        const { track, index } = this.props;
        return (
            <React.Fragment>
										<tr className="PublishingObligationListItem" onMouseOver={this.applyOver.bind(this, index)} onMouseOut={this.applyOut.bind(this)} >
											<td className="PublishingObligationListItem-trackNumber">{parseInt(index + 1)}</td>
											<td className="PublishingObligationListItem-trackName">{track.track_name}</td>
											<td className="PublishingObligationListItem-obligationSelect">
												<FormGroup>
													<div>
														<Select
															className="Select SelectInput-select FormField-form-control PublishingObligationListItemSelect Select--single has-value"
															classNamePrefix="select"
															isSearchable={false}
															options={publishingOb}
															cacheOptions={true}
															isMulti={false}
                                                            onChange={this.handleSelectChange.bind(this)}
                                                            value={this.state.pubOb}
															name="publishing-obligation"
														/>
													</div>
												</FormGroup>


											</td>
											<td className="PublishingObligationListItem-publishersTags">
												<FormGroup>
													<div>
														<Input value={this.state.publisher} onChange={this.handleValueChange.bind(this)} className="TagsInput-input" name="publisher" type="text" />
													</div>
												</FormGroup>
											</td>
											<td className="PublishingObligationListItem-applyAll">

												<TrippleCheckmark setPublisher={this.props.setPublisher.bind(this, this.state.pubOb, this.state.publisher)} showApplyAll={this.state.showApplyAll} value={index} />


											</td>

										</tr>
										<tr className="PublishingObligationListItem-withhold-royalties">
											<td></td>
											<td></td>
											<td colSpan="2"></td>
											<td></td>
										</tr>
									</React.Fragment>
        );
    }
}

export default PublishingTable;