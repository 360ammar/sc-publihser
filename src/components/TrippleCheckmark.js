import React from 'react';
import TooltipCustom from './TooltipCustom';
import {MdDoneAll} from 'react-icons/md';


export default class TrippleCheckmark extends React.Component {
  constructor(props) {
    super(props);


  }

  render() {
    const {showApplyAll,value, setPublisher} = this.props;
    return (
            <div onClick={setPublisher} className={`TripleCheckmark ${!(showApplyAll===value) && 'TripleCheckmark-hidden'}`}>
            <TooltipCustom text="Apply to All" className="TripleCheckmarkTooltip">
            <MdDoneAll className="Icons-triple-checkmark" />
            </TooltipCustom>
            </div>
    );
  }
}