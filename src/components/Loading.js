import React from 'react';
import {  PulseLoader } from 'react-spinners';
export const LoadingIndicator = (isLoading) => { return ( <PulseLoader loading={isLoading} /> ) }