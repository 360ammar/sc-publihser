import React from 'react';
import {Col} from 'reactstrap';
export const ProjectHeader = ({ projectdetails }) => {

  return (
    <Col md="12">
      <header className="ProjectManager-header">
        <article>
           <p className="ProjectHeader-artist">{projectdetails.artist}</p>
           <h1 className="ProjectHeader-name">{projectdetails.name}</h1>
           <p className="ProjectHeader-code">{projectdetails.code}</p>
        </article>
      </header>
    </Col>
  );
};


export default ProjectHeader;
