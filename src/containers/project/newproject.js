import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input, FormFeedback, FormText, Alert } from 'reactstrap';
import Select from 'react-select';
import TooltipCustom from '../../components/TooltipCustom';
import { artist } from '../../actions/artist';
import { project } from '../../actions/project';
import { connect } from 'react-redux';
import {get} from 'lodash';
import { PulseLoader } from 'react-spinners';

class CreateProjectContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			id : null,
			showAddNewArtist: false,
			newArtist: '',
			project_name: '',
			project_artist: undefined,
			code: '',
			description: '', 
			errors: { 
				code: false,
				project_artist: false,
				project_name: false,
			}
		};


		
		this.handleValueChange = this.handleValueChange.bind(this);
		this.onSubmitProject = this.onSubmitProject.bind(this);
		this.handleArtistChange = this.handleArtistChange.bind(this);

	}

	handleValueChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	toggleAddArtist() {
		this.setState({ showAddNewArtist: !this.state.showAddNewArtist })
	}

	handleArtistChange(selected_artist) {
		this.setState({ project_artist: selected_artist })
	}

	componentDidMount(){
//		artist.getArtist(this.props.auth.user.publisher.id, "");
		this.props.getArtists(this.props.auth.user.publisher.id, "");
	}
	onSubmitProject(e) {
		e.preventDefault();
		const { project_artist, project_name, code, description } = this.state;
		
		let errors = { 
			code: false,
			project_artist: false,
			project_name: false,
		}
		
		if (project_artist === undefined) {
			errors.project_artist = true;
		}
		if (project_name === '') {
			errors.project_name = true;
		}
		if (code === '') {
			errors.code = true;
		}
		this.setState({ errors: errors });
		let data = { project_artist: project_artist && project_artist.value , project_name, code, description, publisherId: this.props.auth.user.publisher.id };
		if(!(errors.code || errors.project_artist || errors.project_name))
			this.props.addProject(data);
	}
	

	createArtist(e) {
		e.preventDefault();
		
		this.props.addArtist({ name :this.state.newArtist, publisherId: this.props.auth.user.publisher.id});
	}
	render() {
		const { artist,project } = this.props;
		const artists = get(artist, 'artists.artistPublisher.docs', []);
		console.log('project',get(project, 'addProjectLoading', false))
		return (
			<React.Fragment>
				{ !(get(project, 'addProjectLoading', false))  ?
			 <div className="ProjectLandingPage">
				<Col className="ProjectLandingPage-selectors">
					<Col>
						<h1 className="ProjectLandingPage-header">Create New Project</h1>
						<Form onSubmit={this.onSubmitProject.bind(this)} className="ProjectForm">
							<FormGroup className={`${this.state.errors.project_name && 'FormField-group-has-error' }`} required>
								<Label className="FormField-label" for="project_name">Project Name * <TooltipCustom text="This is typically the name of the album. Example: 'Abbey Road'" /></Label>
								<Input className="FormField-form-control" placeholder="Project Name" value={this.state.project_name} name="project_name" onChange={this.handleValueChange} invalid={this.state.errors.project_name} />
								<FormFeedback>This information is required</FormFeedback>
							</FormGroup>
							<FormGroup className={`${this.state.errors.code && 'FormField-group-has-error' }`} required >
								<Label className="FormField-label" for="project-code">Project Code * <TooltipCustom text="This is a unique code used to identify this product. Example: APPL123CD" /></Label>
								<Input  className="FormField-form-control" placeholder="Project Code" onChange={this.handleValueChange} value={this.state.code} name="code" invalid={this.state.errors.code} />
								<FormFeedback>This information is required</FormFeedback>
							</FormGroup>
							<FormGroup required className={`ProjectArtist ${this.state.errors.project_artist && 'FormField-group-has-error'}`}>
								<Label className="FormField-label" for="project-aritst">Project Artist * <TooltipCustom text="Select the artist to file this project under in Workstation (Artist Builder, Accounting, Analytics, etc). This field is for internal use and will not be displayed at stores." /></Label>
								<div>
									<span className="SelectInputWithAddNew-container">
										<div><Select
											className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
											classNamePrefix="select"
											isSearchable={true}
											name="project_artist"
											value={this.state.project_artist}
										
											matchPos="any"
											matchProp="any"
											options={artists.map((artist) => {
												return { 'label': artist.Artist.name, 'value': artist.Artist.id }
											})}
											onChange={this.handleArtistChange}
										/>
										</div>
										{this.state.errors.project_artist == true && <div className="invalid-feedback">This information is required</div>}
										

										<Button onClick={this.toggleAddArtist.bind(this)} className="SelectInputWithAddNew-add-button" color="link">+ New Artist</Button>
										{this.state.showAddNewArtist ?
											(this.props.artist.creatingArtist ? <div className="spinner">
												<div className="bounce1"></div>
												<div className="bounce2"></div>
												<div className="bounce3"></div>
												<div className="bounce4"></div>
												<div className="bounce5"></div>
											</div> : <article className="SelectInputWithAddNew-new-box">
													<FormGroup id="project-artist-add">
														<Label className="FormField-label" for="artistToAdd">Artist Name</Label>
														<Input className="FormField-form-control" onChange={this.handleValueChange.bind(this)} value={this.state.newArtist} id="artistToAdd" placeholder="Artist Name" name="newArtist" />
														<span className="FormField-note"></span>
													</FormGroup>
													<div className="SelectInputWithAddNew-button-wrapper">
														<Button onClick={this.toggleAddArtist.bind(this)} type="button" className="btn btn-link">Cancel</Button>
														<Button type="button" onClick={this.createArtist.bind(this)} disabled={this.state.newArtist === ''} className="btn btn-default">Add artist</Button>
													</div>
													<div className="loader"></div>

												</article>)
											: null
										}
									</span>
								</div>

							</FormGroup>
							<FormGroup >
								<Label className="FormField-label" for="description">Project Description <TooltipCustom text="This will be used in the Description section of Sales Sheets and other marketing materials. It should be specific to the project and not the different product formats." /></Label>
								<Input  className="FormField-form-control" type="textarea" value={this.state.description} onChange={this.handleValueChange} name="description" id="project-description" />
							</FormGroup>
							<hr />
							<Button className="ProjectForm-save-button btn btn-primary">Create Project</Button>

						</Form>
					</Col>
				</Col>
									</div> : <div className="content-loader" >
				<PulseLoader  loading  color={'#9b9b9b'} size={30} margin={'10px'}/>
				</div> }
						</React.Fragment>			);
	}
}


function mapStateToProps(state) {
	return {
		artist: state.artist,
		artists: state.artists,
		auth: state.auth,
		ui: state.ui,
		
	}
}

function mapDispatchToProps(dispatch) {
    return({
		addArtist: (name, publisherId) => {dispatch(artist.addArtist(name, publisherId))},
		getArtist: (publisher, name) => {dispatch(artist.getArtist(publisher, name))},
		getArtists: (publisher, name) => {dispatch(artist.getArtists(publisher, name))},
		addProject: (data) => {dispatch(project.addProject(data))}, 
    })
}


const NewProject = connect(mapStateToProps, mapDispatchToProps)(CreateProjectContainer);
export default NewProject;