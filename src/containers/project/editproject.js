import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input, FormFeedback, FormText, Alert } from 'reactstrap';
import Select from 'react-select';
import TooltipCustom from '../../components/TooltipCustom';
import { artist } from '../../actions/artist';
import { project } from '../../actions/project';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { get } from 'lodash';
import { PulseLoader } from 'react-spinners';

class EditProjectContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			id: props.match.params.id,
			showAddNewArtist: false,
			newArtist: '',
			project_name: '',
			project_artist: {},
			code: '',
			description: '',
			errors: {
				code: false,
				project_artist: false,
				project_name: false,
			}
		};



	


		this.handleValueChange = this.handleValueChange.bind(this);
		this.onSubmitProject = this.onSubmitProject.bind(this);
		this.handleArtistChange = this.handleArtistChange.bind(this);

	}

	handleValueChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	toggleAddArtist() {
		this.setState({ showAddNewArtist: !this.state.showAddNewArtist })
	}

	handleArtistChange(selected_artist) {
		this.setState({ project_artist: selected_artist })
	}

	componentDidMount() {
		if (this.state.id != null && !(isNaN(this.state.id))) {
			console.log('getProject in container');
			this.props.getProject(this.state.id);
		}
		//		this.props.dispatch(artist.getArtist(this.props.auth.user.publisher.id));
	}
	componentWillReceiveProps() {
		const { projectData } = this.props;
		const {project_data} = projectData.project;
		if (this.state.id && project) {
			const {
				project_name,
				project_artist,
				code,
				description } = project_data;

			this.setState({
				project_name, project_artist : 
					{ label : project_artist.label , value: project_artist.value  }
				, code, description
			});
		}
	}

	onSubmitProject(e) {
		e.preventDefault();
		/*const { project_artist, project_name, code, description } = this.state;
		let errors = Object.assign({}, this.state.errors);
		if (project_artist === undefined) {
			errors.project_artist = true;
		}
		if (project_name === '') {
			errors.project_name = true;
		}
		if (code === '') {
			errors.code = true;
		}
		this.setState({ errors });
		let data = { project_artist: project_artist && project_artist.value , project_name, code, description, publisherId: this.props.auth.user.publisher.id };
		
		if((this.state.errors.code || this.state.errors.project_artist || this.state.errors.project_name))
			this.props.dispatch(project.addProject(data));*/
	}


	createArtist(e) {
		e.preventDefault();
		this.props.dispatch(artist.addArtist(this.state.newArtist, this.props.auth.user.publisher.id));
	}

	render() {
		if (this.state.id == "create-new")
			return (<React.Fragment></React.Fragment>);
		console.log('new_project render');
		const { projectData } = this.props;
		var project = projectData.project;
		console.log(project);
		const { showAddNewArtist } = this.state;
		const project_data = get(project, 'project_data', null);
		console.log(project_data);
		if (project_data != null) {
			var pdata = project.project_data;
			//			this.setState( {project_name: pdata.project_name}) ;
		}
		//		console.log((!!(this.props.match.params.id) && (get(project, 'project_data',false))));
		return (

			<React.Fragment>
				{ this.props.projectData.getProjectLoading ? 
					<div className="content-loader" >
					<PulseLoader   loading={this.props.projectData.getProjectLoading}  color={'#9b9b9b'} size={30} margin={'10px'}/>
					</div>
					 : <div className="ProjectLandingPage">
						{projectData.onNewProjectSuccess || (project && get(project, 'project_data.project_albums', 0).length === 0) ? <Alert color="success">
							Project Successfully Updated.
					  </Alert> : null}
					  {projectData.getProjectError ? <Alert color="dange">
					  There was an unknown error occurred. Please try again and if the problem persist contact the support.
      				</Alert> : null}
						<Col className="ProjectLandingPage-selectors">
							<Col>
								<h1 className="ProjectLandingPage-header">Edit Project</h1>
								<form onSubmit={this.onSubmitProject.bind(this)} className="ProjectForm">
									<FormGroup className={`${this.state.errors.project_name && 'FormField-group-has-error'}`} required>
										<Label className="FormField-label" for="project_name">Project Name * <TooltipCustom text="This is typically the name of the album. Example: 'Abbey Road'" /></Label>
										<Input defaultValue={project_data && project_data.project_name} className="FormField-form-control" placeholder="Project Name" value={this.state.project_name} name="project_name" onChange={this.handleValueChange} invalid={this.state.errors.project_name} />
										<FormFeedback>This information is required</FormFeedback>
									</FormGroup>
									<FormGroup className={`${this.state.errors.code && 'FormField-group-has-error'}`} required >
										<Label className="FormField-label" for="project-code">Project Code * <TooltipCustom text="This is a unique code used to identify this product. Example: APPL123CD" /></Label>
										<Input defaultValue={project_data && project_data.code} className="FormField-form-control" placeholder="Project Code" onChange={this.handleValueChange} value={this.state.code} name="code" invalid={this.state.errors.code} />
										<FormFeedback>This information is required</FormFeedback>
									</FormGroup>
									<FormGroup required className={`ProjectArtist disabled-element ${this.state.errors.project_name && 'FormField-group-has-error'}`}>
										<Label className="FormField-label" for="project-aritst">Project Artist * <TooltipCustom text="Select the artist to file this project under in Workstation (Artist Builder, Accounting, Analytics, etc). This field is for internal use and will not be displayed at stores." /></Label>
										<div>
											<span className="SelectInputWithAddNew-container">
												<div><Select
													disabled
													className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
													classNamePrefix="select"
													isSearchable={true}
													name="project_artist"
													value={this.state.project_artist}
													defaultOption={project_data && project_data.project_artist}
													matchPos="any"
													matchProp="any"
													options={this.props.artist.artist.artistPublisher && this.props.artist.artist.artistPublisher.docs.map((artist) => {
														return { 'label': artist.Artist.name, 'value': artist.Artist.id }
													})}
													onChange={this.handleArtistChange}
												/>
												</div>
												{this.state.errors.project_artist && <div className="invalid-feedback">This information is required</div>}


												<Button disabled onClick={this.toggleAddArtist.bind(this)} className="SelectInputWithAddNew-add-button" color="link">+ New Artist</Button>
												{showAddNewArtist ?
													(this.props.artist.creatingArtist ? <div className="spinner">
														<div className="bounce1"></div>
														<div className="bounce2"></div>
														<div className="bounce3"></div>
														<div className="bounce4"></div>
														<div className="bounce5"></div>
													</div> : <article className="SelectInputWithAddNew-new-box">
															<FormGroup id="project-artist-add">
																<Label className="FormField-label" for="artistToAdd">Artist Name</Label>
																<Input className="FormField-form-control" onChange={this.handleValueChange.bind(this)} value={this.state.newArtist} id="artistToAdd" placeholder="Artist Name" name="newArtist" />
																<span className="FormField-note"></span>
															</FormGroup>
															<div className="SelectInputWithAddNew-button-wrapper">
																<Button onClick={this.toggleAddArtist.bind(this)} type="button" className="btn btn-link">Cancel</Button>
																<Button type="button" onClick={this.createArtist.bind(this)} disabled={this.state.newArtist === ''} className="btn btn-default">Add artist</Button>
															</div>
															<div className="loader"></div>

														</article>)
													: null
												}
											</span>
										</div>

									</FormGroup>
									<FormGroup >
										<Label className="FormField-label" for="description">Project Description <TooltipCustom text="This will be used in the Description section of Sales Sheets and other marketing materials. It should be specific to the project and not the different product formats." /></Label>
										<Input defaultValue={project_data && project_data.description} className="FormField-form-control" type="textarea" value={this.state.description} onChange={this.handleValueChange} name="description" id="project-description" />
									</FormGroup>
									<hr />
									<Button className="ProjectForm-save-button btn btn-primary">Save Project</Button>

								</form>
							</Col>
						</Col>
					</div> }

			</React.Fragment>);
	}
}



function mapStateToProps(state) {
	return {
		artist: state.artist,
		auth: state.auth,
		ui: state.ui,
		projectData: state.project
	}
}

function mapDispatchToProps(dispatch) {
	return ({
		getProject: (id) => { dispatch(project.getProject(id)) },
	})
}


const EditProject = connect(mapStateToProps, mapDispatchToProps)(EditProjectContainer);
export default EditProject;