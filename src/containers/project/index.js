import React, { Component } from 'react';
import {Row, Col,Nav, NavItem, Button, UncontrolledAlert} from 'reactstrap';
import ProjectHeader from '../../components/project/project-header';
import NewProject from './newproject.js';
import EditProject from './editProject.js';
import {connect} from 'react-redux';
import ProductOverview from './product/overview';
import CreateProduct from './product/create';
import ProductBasics from './product/basics';
import ProductArtwork from './product/artwork';
import ProductTracks from './product/tracks';
import ProductPubObligations from './product/publishingObligations';
import ProductMarketing from './product/marketing'
import {ProductSidebar} from './product/sidebar';
import {NavLink, Route, Link } from 'react-router-dom';
import { project } from '../../actions/project';
import {PulseLoader} from 'react-spinners';
import {get} from 'lodash'
class ProjectContainer extends Component {

  constructor(props){
    super(props);
    this.state = { 
      newproductNav : null,
      activeProduct : '',
      errorOccured : false
    };
  this.setActiveProduct = this.setActiveProduct.bind(this)
  }

  componentDidUpdate(prevProps,prevState)
  {
    if((prevState.errorOccured !== this.state.errorOccured) && (this.props.project.getProjectError || this.props.product.error)){
      this.setState({errorOccured : true})
    }
  }

  componentDidMount(){
    this.props.match.params.id && !(isNaN(this.props.match.params.id)) && this.props.getProject(this.props.match.params.id);
    const product = (this.props.location.pathname).split('/');
    this.setState({ activeProduct : Number(product[(product.indexOf('product')+1)]) })  
  }

  // onNewProduct (e) {
  //   e.preventDefault();
  //   console.log('onClick');
  //   this.setState({newproductNav: 

  //     <NavItem className="ProjectNavigation-product-wrapper">
  //             <NavLink className="Opm-navigation-link" activeClassName="Opm-navigation-selected" to="/project/product/create">
  //             <div className="Opm-navigation-container"><h4 className="Opm-navigation-format">New Product</h4></div>
  //             </NavLink>
  //           </NavItem>

  //   })
  // }

  setActiveProduct(id){
    this.setState({
      activeProduct : id === this.state.activeProduct ? '' : id
    });
  }

  // componentDidMount(){
  //   this.props.location.pathname.includes('product') && this.setState({
  //     activeProduct : (this.props.location.pathname.split('/')[4] ? parseInt(this.props.location.pathname.split('/')[4]) : '')
  //   });
  // }


  render() {
     const {activeProduct } = this.state;
        let products = get(this.props.project ,'project.project_data.project_products', []);
    return (
    <React.Fragment> {this.props.project.getProjectLoading && <div className="ProjectManager-wrapper content-loader bg-white" ><PulseLoader  loading={this.props.project.getProjectLoading}   color={'#9b9b9b'} size={30} margin={'10px'}/></div> } 
      <div className={`ProjectManager-wrapper ${this.props.project.getProjectLoading && 'invisible'}`}>
     <Row>
     
    {!(this.props.location.pathname.includes('/create-new')) &&
      this.props.project.getProjectLoading ? <div className="content-loader" ><PulseLoader  loading={this.props.project.getProjectLoading}   color={'#9b9b9b'} size={30} margin={'10px'}/></div> 
    : <ProjectHeader projectdetails={{name: this.props.project.project.project_data.project_name, code: this.props.project.project.project_data.code, artist: this.props.project.project.project_data.project_artist.label }} /> }
      </Row>
      { this.state.errorOccured && <UncontrolledAlert>An unknown error occured! Please try again.</UncontrolledAlert> }
     
      <Row className="ProjectManager-main">
      <Col md="3" className="no-pad">
      <aside className="ProjectManager-aside">
      <nav>
        <Nav vertical className="ProjectNavigation-list-wrapper">
            <NavItem className="ProjectNavigation-product-wrapper">
            <NavLink exact  className="Opm-navigation-link" activeClassName="Opm-navigation-selected" to={`/project/${this.props.match.params.id ? this.props.match.params.id : 'create-new' }`}>
              <div className="Opm-navigation-container"><h4 className="Opm-navigation-format">Project</h4></div>
              </NavLink>
              
            </NavItem>


            {this.props.ui.isNewProduct && <NavItem className="ProjectNavigation-product-wrapper">
              <NavLink className="Opm-navigation-link" activeClassName="Opm-navigation-selected" to={`/project/${this.props.match.params.id}/product/create`}>
              <div className="Opm-navigation-container"><h4 className="Opm-navigation-format">New Product</h4></div>
              </NavLink>
            </NavItem>}
          {products.map((product)=> <ProductSidebar active={product.id === activeProduct} projectid={this.props.match.params.id} setActiveProduct={this.setActiveProduct} product={product} key={product.id} />)}

          
          </Nav>
       </nav> 
       
      <div className="ProjectManager-button-container" >
        <Link className={this.props.location.pathname.includes('/create-new') && 'disabled-element'} disabled={this.props.location.pathname.includes('/create-new')} to={`/project/${this.props.match.params.id}/product/create`} className="btn btn-default btn-lg ProjectManager-new-product" >New Product</Link>
      </div>
      </aside>
      </Col>
      

      <Col md="9" className="no-pad">
      <article className="ProjectManager-content"> 
        <Route exact path={'/project/create-new'} component={NewProject} />
        <Route exact path={'/project/:id'} render={(props) => <EditProject {...props} />} />
        <Route path={'/project/:id/product/create'} render={(props) => <CreateProduct {...props} />} />
        <Route path={'/project/:id/product/:productId/overview'} render={(props) => <ProductOverview {...props} />} />
        <Route path={'/project/:id/product/:productId/basics'} render={(props) => <ProductBasics {...props} />} />
        <Route path={'/project/:id/product/:productId/artwork'} render={(props) => <ProductArtwork {...props} />} />
        <Route path={'/project/:id/product/:productId/tracks'} render={(props) => <ProductTracks {...props} />} />
        <Route path={'/project/:id/product/:productId/marketing'} render={(props) => <ProductMarketing {...props} />} />
        <Route path={'/project/:id/product/:productId/publishing-obligations'} render={(props) => <ProductPubObligations {...props} />} />
      </article>   
      </Col>
    </Row> 
      </div>     </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return({
  getProject: (id) => {dispatch(project.getProject(id))}, 
  })
}


function mapStateToProps(state){
    return {
		artist: state.artist,
		auth: state.auth,
        ui: state.ui,
        project: state.project,
        product: state.product
        
	}
}
export default connect(mapStateToProps,mapDispatchToProps)(ProjectContainer);