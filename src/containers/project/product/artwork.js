import React from 'react';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import {product} from '../../../actions/product';
import { PulseLoader } from 'react-spinners'
import { get } from 'lodash'
import {Form , UncontrolledAlert} from 'reactstrap'
const dropzoneRef = React.createRef();
class ProductArtwork extends React.Component {

  constructor(props) {
    super(props);
    this.state = { accepted: [], rejected: [], errFileType: false, imgWidth: undefined, imgHeight: undefined, imgSrc : null };
  }

  componentDidMount() {
    this.props.getProduct(this.props.match.params.productId);
  }

  componentDidUpdate(prevProps) {
    if(this.props.product !== prevProps.product){
      const artwork = get(this.props.product, 'userAlbum.result', false);
      !!artwork && this.state.imgSrc === null && this.setState({ imgSrc : artwork.artwork ? `https://api.shiacloud.net/${artwork.artwork}` : null })
    }
  }

  submitUpload(e){
    e.preventDefault();
    const formData = new FormData(e.target);

    formData.append('artwork-file', this.state.accepted[0]);
    formData.append('productid', this.props.match.params.productId);

    for (var key of formData.entries()) {
      console.log(key[0] + ', ' + key[1]);
    }

    return this.props.addArtwork(formData);

  }

  onDrop(accepted, rejected) {
    const self = this;
    this.setState({
      accepted, rejected
    });



    if (accepted[0] && accepted) {
      console.log(accepted[0])
      var img = new Image();

      img.onload = function () {
        console.log(this.width + " " + this.height);
        self.setState({
          imgWidth: this.width,
          imgHeight: this.height
        });

      };

      var reader = new FileReader();
      reader.onloadend = function (ended) {
        img.src = ended.target.result;
        self.setState({imgSrc: ended.target.result
        });
      }
      reader.readAsDataURL(accepted[0]);
    }


    if (rejected.length !== 0) this.setState({ errFileType: true, imgSrc: null });
  }

  render() {

    const ArtworkUpload = get(this.props.product , 'artwork_upload.artwork_upload' , false);

    return (
      this.props.product.fetchingProduct
      ?
      <div className="content-loader" >
        <PulseLoader  loading={this.props.product.fetchingProduct}  color={'#9b9b9b'} size={30} margin={'10px'}/>
      </div>
      :
      <div className="ProductArtwork">
        <h2 className="ProductArtwork-heading">Artwork</h2>
        {!!ArtworkUpload && <UncontrolledAlert>Artwork Upload Success!</UncontrolledAlert>}
        <Form onSubmit={this.submitUpload.bind(this)} >
        <section className="ArtworkUpload ArtworkUpload--error">


          <Dropzone className="ArtworkUpload-dropzone" aria-disabled="false"
            accept="image/jpeg,image/jpg,image/tiff,image/tif"
            ref={dropzoneRef}
            onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
          >
            <button type="button" className="btn btn-default">
              Upload artwork
		        </button>

          </Dropzone>


          {(this.state.errFileType || (this.state.imgHeight !== this.state.imgWidth) || (this.state.imgHeight < 3000 && this.state.imgWidth < 3000) || (this.state.imgHeight > 6000 && this.state.imgWidth > 6000)) ? <h5 className="ArtworkUpload-spec-heading ArtworkUpload-spec-heading--error">Your image failed for the following reason(s):</h5>
            : <h5 className="ArtworkUpload-spec-heading">Your image must be:</h5>}
          <ul className="ArtworkUpload-spec-container">

            <li className={`${this.state.errFileType && 'ArtworkUpload-spec--error'}`}>TIF or JPG format</li>
            <li className={`${(this.state.imgHeight !== this.state.imgWidth) && 'ArtworkUpload-spec--error'}`}>Square</li>
            <li className={`${(this.state.imgHeight < 3000 && this.state.imgWidth < 3000) && 'ArtworkUpload-spec--error'}`}>Minimum size: 3000 x 3000 pixels</li>
            <li className={`${(this.state.imgHeight > 6000 && this.state.imgWidth > 6000) && 'ArtworkUpload-spec--error'}`}>Maximum size: 6000 x 6000 pixels</li>
          </ul>
          <p>If you’re scanning a CD, please remove any product sticker and crop marks</p>

          { !!(this.state.imgSrc || this.state.accepted.length > 0)  && <div><article className="ArtworkUpload-success" 
        style={{ backgroundImage : "url("+ this.state.imgSrc  +")" }}>
        <PulseLoader loading={this.props.product.uploadingArtwork} size={30} margin={'15px'} color={'#ececec'} />
        </article></div> }
       {!!(this.state.imgSrc) && <button type="button" className="ArtworkUpload-replace-button btn btn-danger" onClick={() => dropzoneRef.current.open()}>
    			    Replace Artwork
    </button> }
        </section>
        <div className="DigitalProductFooter">
          <hr />
          <button  type="button" className="DigitalProductFooter-back-button btn btn-default">

Overview</button>
          <button type="submit" id="digital-product-footer-next-button" className="DigitalProductFooter-button pull-right btn btn-primary ml-auto">
            > SAVE</button>
         
         
        </div>
        </Form>
        </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return({
	  getProduct: (id) => {dispatch(product.getProduct(id))}, 
    addArtwork: (data) => {dispatch(product.addArtwork(data))}, 
  })
}


function mapStateToProps(state) {
  return {
    product: state.product
  }
}


export default connect(mapStateToProps , mapDispatchToProps)(ProductArtwork);