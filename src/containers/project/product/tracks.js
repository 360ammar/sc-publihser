import React from 'react';
import {connect} from 'react-redux';
import Dropzone from 'react-dropzone';
import TrackTable from '../../../components/product/tracks/TrackTable';
import TrackForm from '../../../components/product/tracks/trackForm';
import { track } from '../../../actions/track'
import { product } from '../../../actions/product'
import { get, find } from 'lodash'
import { PulseLoader } from 'react-spinners'

class ProductTracks extends React.Component {

	constructor(props){
		super(props);
		this.state = { trackErr  : false, trackLength : 0 , activeItem: null , trackIndex : 1, accepted: [] };
		this.itemClicked = this.itemClicked.bind(this)
		this.uploadTrack = this.uploadTrack.bind(this)
		this.onDrop = this.onDrop.bind(this)
	}

	componentDidMount() {
    this.props.getProduct(this.props.match.params.productId);
	}
	
	componentDidUpdate(prevProps) {
		if(this.props.track !== prevProps.track){
			if(this.props.track.IndexesUpdateSuccess){
				this.props.track.IndexesUpdateSuccess.track && this.props.getProduct(this.props.match.params.productId);			
			}
		}
	}

 onDrop(accepted, rejected) {
				const self = this;
				this.setState({
            accepted, rejected
        });
					console.log(accepted)
				if(accepted.length > 0) {
					self.uploadTrack(accepted);
				}

        if(rejected.length !== 0)  this.setState({trackErr: true});
		}
		
		uploadTrack (accepted) {
				accepted.forEach(trackFile=> {
					let formData = new FormData();
					formData.append('track-file', trackFile);
					formData.append('productId', this.props.match.params.productId )
					formData.append('publisherId', this.props.user.publisher.id )
					this.props.uploadTrack(formData);
				});
		}

		itemClicked ({item, index}) {
			console.log('item clicked' , item);
			this.props.getTrack(item);
			this.setState({ activeItem : item, trackIndex: index });
		}



  render() {

		const {activeItem, trackIndex, accepted } = this.state;

		const tracks = get(this.props.product, 'userAlbum.result.albumtracks' , []);
		const Uploadedtrack = get(this.props.track , 'track.track_upload' , []);
		const currentTrack = get(this.props.track , 'selectedTrack.track' , false)
		const { track } = this.props;

    return (
			this.props.product.fetchingProduct || tracks.length === 0
      ?
      <div className="content-loader" >
        <PulseLoader  loading={this.props.product.fetchingProduct}  color={'#9b9b9b'} size={30} margin={'10px'}/>
      </div>
      :
		<div>
			<div className="TrackBuilder-header">
				<h2 className="TrackBuilder-header-label">Tracks</h2>
				<span className="TrackBuilder-header-space"></span>
			</div>

		<div className="TrackUpload">
		
	 	{this.state.trackErr ?	<div className="UploadErrorMessage">
	 					<div className="UploadErrorMessage-content">
	 						<h3 className="UploadErrorMessage-header">Error uploading track(s)</h3>
	 						<ul className="UploadErrorMessage-files">
	 							<li><span className="UploadErrorMessage-filename">SampleAudio_0.7mb.mp3</span>
	 								<span> is not an accepted format.</span>
	 							</li>
	 						</ul>
	 						<span className="TrackUpload-fileformats">What file formats can I upload?</span>
	 					</div>
	 				</div> :  null }
	 	{ (Uploadedtrack.length > 0 || tracks.length > 0  || track.uploadingTrack) ?
				<div className="HeaderButtons">
    		   <Dropzone className="HeaderButtons-upload" aria-disabled="false"
		            accept="audio/mpeg,audio/wav"
		            onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
		          		>
          		<button type="button" className="btn btn-default">
    			    Upload Tracks
		        </button>
      	
          		</Dropzone>
          		</div> : 	<div className="TrackUpload-dropzone" aria-disabled="false">
	 					<div className="text-center">
	 						<div className="TrackUpload-instructions">
	 							<p><b>Drag and Drop</b> to upload audio files or add tracks using the buttons below</p>
	 				 			<div className="TrackUpload-dropzone-helpmessage">
	 				 			<span className="TrackUpload-fileformats">What file formats can I upload?</span>
	 				 			</div>
	 				 		</div>
	 						   <Dropzone className="HeaderButtons-upload" aria-disabled="false"
		            accept="audio/mpeg,audio/wav"
		            onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
		          		>
          		<button type="button" className="btn btn-default">
    			    Upload Tracks
		        </button>
      	
          		</Dropzone>
	 				 	</div>
	 				</div>
          		 }



		</div>  

		{ (Uploadedtrack.length > 0 || tracks.length > 0 || track.uploadingTrack) &&

		<div className="TrackBuilder-panels">
			<TrackTable files={accepted} tracks={tracks} updateIndex={this.props.updateIndex} uploadedTrack={Uploadedtrack} uploading={track.uploadingTrack} itemClicked={this.itemClicked.bind(this)} activeItem={activeItem} />
			<div className="TrackBuilder-editor-wrapper">
			{ !!(activeItem) && !track.fetchingTrack &&  currentTrack ? <TrackForm activeItem={activeItem} uploading={track.formUploading} uploadedTrack={track.formUploaded} track={currentTrack} fetchingTrack={track.fetchingTrack} user={this.props.user} uploadFormTrack={this.props.uploadFormTrack} productId={this.props.match.params.productId} trackIndex={trackIndex} />:
		   track.fetchingTrack && <div className="content-loader" >
			 <PulseLoader  loading  color={'#9b9b9b'} size={30} margin={'10px'}/>
		 </div>
		}
			</div>
		</div>

		 }
			<div className="DigitalProductFooter">
				<hr />
				{/* <button type="button" id="digital-product-footer-next-button" className="DigitalProductFooter-button pull-right btn btn-primary">
				SAVE
				</button> */}
				<button type="button" className="DigitalProductFooter-back-button btn btn-default">
				Overview
				</button>
			</div>
		</div>
    );		
  }
}

function mapStateToProps(state) {
    return {
			product : state.product,
			track : state.track,
			user : state.auth.user
		}
}


function mapDispatchToProps(dispatch) {
	return({
		getProduct: (id) => {dispatch(product.getProduct(id))}, 
		uploadTrack: (trackFile) => {dispatch(track.uploadTrack(trackFile))},
		uploadFormTrack : (formData) => {dispatch(track.uploadFormTrack(formData))},
		getTrack: (id) => {dispatch(track.getTrack(id))},
		updateIndex: (indexes) => {dispatch(track.updateIndex(indexes))}
	})
}


export default connect(mapStateToProps,mapDispatchToProps)(ProductTracks);