import React from 'react';
import {connect} from 'react-redux';
import FilteredMultiSelect from 'react-filtered-multiselect';
import {Row, Col} from 'reactstrap';
import {product} from '../../../actions/product'
import { get } from 'lodash';
import { PulseLoader } from 'react-spinners'

const Platforms = [
  {
    "name": "24/7 Entertainment GmbH",
    "id": 1
  },
  {
    "name": "7 Digital",
    "id": 2
  },
  {
    "name": "Amazon Digital Services Inc.",
    "id": 3
  },
  {
    "name": "Anghami",
    "id": 4
  },
  {
    "name": "Audible Magic",
    "id": 5
  },
  {
    "name": "Audible Magic (Music Rights ID)",
    "id": 6
  },
  {
    "name": "AWA",
    "id": 7
  },
  {
    "name": "Bugs Corporation",
    "id": 8
  },
  {
    "name": "BuzzAngle",
    "id": 9
  },
  {
    "name": "Deezer",
    "id": 10
  },
  {
    "name": "Facebook",
    "id": 11
  },
  {
    "name": "Fizy",
    "id": 12
  },
  {
    "name": "Google Music",
    "id": 13
  },
  {
    "name": "HD Tracks",
    "id": 14
  },
  {
    "name": "HighRes Audio",
    "id": 15
  },
  {
    "name": "iTunes/Apple",
    "id": 16
  },
  {
    "name": "Jaxsta",
    "id": 17
  },
  {
    "name": "Library Ideas / Freegal",
    "id": 18
  },
  {
    "name": "MediaNet",
    "id": 19
  },
  {
    "name": "Napster",
    "id": 20
  },
  {
    "name": "Neurotic Media",
    "id": 21
  },
  {
    "name": "Pandora",
    "id": 22
  },
  {
    "name": "PERSONAL MUSICA",
    "id": 23
  },
  {
    "name": "ProStudioMasters.com Inc.",
    "id": 24
  },
  {
    "name": "Qobuz",
    "id": 25
  },
  {
    "name": "Reliance Jio",
    "id": 26
  },
  {
    "name": "Saavn",
    "id": 27
  },
  {
    "name": "Shazam",
    "id": 28
  },
  {
    "name": "Simfy Africa",
    "id": 29
  },
  {
    "name": "Slacker",
    "id": 30
  },
  {
    "name": "SoundCloud Fingerprinting",
    "id": 31
  },
  {
    "name": "Superplayer",
    "id": 32
  },
  {
    "name": "TIDAL",
    "id": 33
  },
  {
    "name": "UMA",
    "id": 34
  },
  {
    "name": "Vervelife",
    "id": 35
  },
  {
    "name": "Yandex LLC",
    "id": 36
  },
  {
    "name": "YG Plus",
    "id": 37
  },
  {
    "name": "YouTube Art Tracks & Music Videos",
    "id": 38
  },
  {
    "name": "YouTube Content ID",
    "id": 39
  },
  {
    "name": "Zed Russia",
    "id": 40
  },
  {
    "name": "Zvooq",
    "id": 41
  }
];

const BOOTSTRAP_CLASSES = {
  filter: 'form-control',
  select: 'form-control marketing-select',
  button: 'btn btn btn-block btn-default',
  buttonActive: 'btn btn btn-block btn-primary',
}

class ProductMarketing extends React.Component {

	constructor(props){
		super(props);
    this.state = {selectedShips: []};
    this.handleSubmit = this.handleSubmit.bind(this); 
	}
  handleDeselect(index) {
    var selectedShips = this.state.selectedShips.slice()
    selectedShips.splice(index, 1)
    this.setState({selectedShips})
  }
  componentDidMount() {
      this.props.getMarketing(this.props.match.params.productId);
  }

  handleSubmit () {
    this.props.addMarketing(JSON.stringify(this.state.selectedShips) , this.props.match.params.productId)
  }

  handleSelectionChange (selectedShips) {
    this.setState({selectedShips})
  }

  componentDidUpdate(prevProps) {
    if(prevProps.marketing.getMarketing && (prevProps.marketing.getMarketing !== this.props.getMarketing) && Object.keys(this.props.marketing.marketing).length > 0) {
      const selected = get(this.props.marketing, 'marketing.response.marketing' , []);
      this.setState({ selectedShips : JSON.parse(selected) })
    }
  }

  render() {
  	var {selectedShips} = this.state
    return (
    this.props.marketing.getMarketing || this.props.marketing.addMarketing  ? <div className="content-loader" ><PulseLoader  loading   color={'#9b9b9b'} size={30} margin={'10px'}/></div>  :  <div>
		<h2><span>Marketing</span></h2>
		<h3><span>Select the platforms where you want to distribute your content.</span></h3>
		<Row style={{paddingTop: "10px"}}>
		<Col>

		 <FilteredMultiSelect
        onChange={this.handleSelectionChange.bind(this)}
        options={Platforms}
        className='Opm-thin-grey-border Opm-border-radius'
        classNames={BOOTSTRAP_CLASSES}
        selectedOptions={selectedShips}
        textProp="name"
        valueProp="id"
        style={{ width: "45%"  }}
      />
      </Col>
      <Col>
      {selectedShips.length === 0 && <p>(nothing selected yet)</p>}
      {selectedShips.length > 0 && <ul className="marketing-list">
        {selectedShips.map((ship, i) => <li key={ship.id}>
          {`${ship.name} `}
          <button className="btn btn-secondary"  onClick={() => this.handleDeselect(i)}>Remove</button>
          
       
        </li>)}
      </ul>}
      </Col>
      </Row>
	<div className="DigitalProductFooter">
				<hr />
				<button type="button" id="digital-product-footer-next-button" onClick={this.handleSubmit} className="DigitalProductFooter-button pull-right btn btn-primary">
				SAVE
				</button><button type="button" className="DigitalProductFooter-back-button btn btn-default">
				Overview
				</button>
			</div>
		</div>
    );		
  }
}

function mapDispatchToProps(dispatch) {
  return ({
    addMarketing: (marketing,productId) => {dispatch(product.addMarketing(marketing,productId))},
    getMarketing: (productId) => {dispatch(product.getMarketing(productId))} 
  })
}

function mapStateToProps(state) {
    return {
      marketing : state.product
    };
}


export default connect(mapStateToProps,mapDispatchToProps)(ProductMarketing);