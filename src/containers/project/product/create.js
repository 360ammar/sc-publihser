import React from 'react';
import {Button, Form, FormGroup, Label,ButtonGroup, Input,FormFeedback} from 'reactstrap';
import {connect} from 'react-redux';
import { ui } from '../../../actions/ui'
import TooltipCustom from '../../../components/TooltipCustom';
import {product} from '../../../actions/product'
import { PulseLoader } from 'react-spinners';
import {get} from 'lodash';
class CreateProduct extends React.Component {

	constructor(props){
		super(props);
		this.state={
			format: null,
			name: '',
			product_code : '',
			upc_code : '',
			errors: {
				format: null,
				name: false,
				upc_code : null,
			}
		};
		this.selectedFormat = this.selectedFormat.bind(this);
		this.handleValueChange = this.handleValueChange.bind(this);
	}


	selectedFormat(e, value){
		e.preventDefault();
		this.setState({format : value});
	}
	
	componentWillMount() {
		this.props.onNewProduct(true);
	}
	componentWillUnmount(){
		this.props.onNewProduct(false);	
	}
	componentDidUpdate(prevProps){
		prevProps.product.createProductRequest !== this.props.product.createProductRequest && get(this.props.product ,'newProduct.album.id',false) &&
			this.props.history.replace(`/project/${this.props.match.params.id}/product/${this.props.product.newProduct.album.id}/overview`)
	}
	handleValueChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	onSubmitProduct(e) {
		e.preventDefault();
		const { format, name, product_code, upc_code } = this.state;
		
		let errors = {
			format: null,
			name: false,
			product_code : false,
			upc_code : null,
		}
		
		if (format === null) {
			errors.format = true;
		}
		if (name === '') {
			errors.name = true;
		}
		if (upc_code === '') {
			errors.upc_code = 123;
		}
		if (isNaN(upc_code)) {
			errors.upc_code = true;
		}

		this.setState({ errors: errors });
		let data = { format , name , product_code, upc_code ,publisherId: this.props.auth.id, projectId: this.props.match.params.id };
		if(!(errors.format || errors.upc_code || errors.name))
			this.props.createProduct(data);
	}

  render() {

		const { errors,format, name, product_code, upc_code } = this.state;
		console.log(this.props)
    return (
      !this.props.product.createProductRequest ?	<div className="ProductTypeSelect">
    		<div className="ProductTypeSelect-content">
    			<div className="ProductTypeSelect-headers">
		    		<h2 className="ProductTypeSelect-header">
		    			Create New Product
		    		</h2>
		    	</div>
		    	<section className="BasicProductDetails">
		    	<Form onSubmit={this.onSubmitProduct.bind(this)}>
		    		<div className="BasicProductDetails-format">
		    		<div className="BasicProductFormat">
		    			<FormGroup required className={`${errors.format && 'FormField-group-has-error'}`}>
		    			<Label>Format *</Label>
							<br />
		    			<ButtonGroup>
					        <Button size="lg" onClick={(e) => this.selectedFormat(e, 'Full Length')} className={this.state.format==='Full Length' ? 'ButtonsRow-active' : ''}><div className="ButtonsRow-text">Full Length</div></Button>
					        <Button size="lg" onClick={(e) => this.selectedFormat(e, 'EP')} className={this.state.format ==='EP' ? 'ButtonsRow-active' : ''}><div className="ButtonsRow-text">EP</div></Button>
					        <Button size="lg" onClick={(e) => this.selectedFormat(e, 'Single')} className={this.state.format ==='Single' ? 'ButtonsRow-active' : ''}><div className="ButtonsRow-text">Single</div></Button>
					    </ButtonGroup>
							{errors.format && <div className="invalid-feedback">This information is required</div>}
		    			</FormGroup>

		    		</div>
		    		</div>
		    		<FormGroup required className={`${errors.name && 'FormField-group-has-error'}`}>
		    			<Label for="product-name">Product Name * <TooltipCustom text="The name of the product that will be sent to stores." /></Label>
		    			<Input onChange={this.handleValueChange} type="text" id="product-name" name="name" value={name} invalid={errors.name} />
							<FormFeedback>This information is required</FormFeedback>
		    		</FormGroup>
		    		<FormGroup >
		    			<Label for="product-code">Product Code <TooltipCustom text="This is a unique code used to identify this product. Example: APPL123CD. Also known as Catalog Number. This is required as it's sent through the supply chain." /></Label>
		    			<Input type="text" onChange={this.handleValueChange} id="product-code" value={product_code} name="product_code"/>
		    		</FormGroup>
		    		<FormGroup required className={`${errors.upc_code && 'FormField-group-has-error'}`}>
		    			<Label for="upc-code">UPC * <TooltipCustom text="This is a 12 or 13 digit numeric code that identifies this product. If you do not have a UPC / EAN, one can be assigned for you." /></Label>
		    			<Input type="text" id="upc-code" onChange={this.handleValueChange} name="upc_code" invalid={!!(errors.upc_code)} />
							<FormFeedback><React.Fragment>{(isNaN(errors.upc_code)) ? 'This information is required.' : 'Please enter a valid number.'  }</React.Fragment></FormFeedback>
		    		</FormGroup>
		    		<Button className="ProductTypeSelect-submit-button btn btn-primary" >Create Product</Button>
		    	</Form>
		    	</section>
    		</div>
    	</div>	: 
			<div className="content-loader" >
			<PulseLoader  loading={this.props.product.createProductRequest}   color={'#9b9b9b'} size={30} margin={'10px'}/>
			</div>
    );		
  }
}

function mapDispatchToProps(dispatch) {
    return({
				onNewProduct: (bool) => {dispatch(ui.onNewProduct(bool))},
				createProduct: (data) => {dispatch(product.createProduct(data))}
    })
}

function mapStateToProps(state) {
	return {
		auth: state.auth.user.publisher,
		product: state.product		
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);