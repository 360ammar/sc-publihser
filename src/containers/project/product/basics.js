import React from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, Label, Input, Button , Alert} from 'reactstrap';
import Select from 'react-select';
import CreatableSelect from 'react-select/lib/Creatable';
import {FaAngleLeft, FaAngleRight} from 'react-icons/fa';
import TooltipCustom from '../../../components/TooltipCustom'
import {product} from '../../../actions/product';
import {artist} from '../../../actions/artist'
import {get , debounce } from 'lodash'
import {PulseLoader} from 'react-spinners'
import data from '../../../data'



class ProductBasics extends React.Component {

	constructor(props){
		super(props);
    this.state = { 
      
      meta_language: {},
      localized_language: {},
      release_date: null,
      upc_code: '',
      name: '',
      name_displayed: '',
      product_code: '',
      genre: null,
      sub_genre: null,
      format: '',
      imprint: null,
      copy_line: null,
      product_highlights: null,
      special_instructions: null,
      primary_artists: [],
      featured_artists: [],
      remixers: [],
			producers: [],
			filteredSubGenre : []
  };

	this.handleValueChange = this.handleValueChange.bind(this)
	this.getArtists = debounce(this.getArtists, 500)

  }
  

  componentDidMount(){
		this.props.getProduct(this.props.match.params.productId);
		this.props.getArtists(this.props.auth.user.publisher.id, "");
		this.props.getGenre();
		this.props.getSubGenre();
	}
  componentDidUpdate(prevProps, prevState) {
    (this.props.match.params.productId !== prevProps.match.params.productId) && this.props.getProduct(this.props.match.params.productId);
      if (this.props.product !== prevProps.product && get(this.props.product, 'userAlbum.result', false)) {
        const product_data = get(this.props.product, 'userAlbum.result', false);
        const {
          meta_language,
          localized_language,
          release_date,
          upc_code,
          name,
          name_displayed,
          product_code,
          genre,
          sub_genre,
          format,
          imprint,
          copy_line,
          product_highlights,
          special_instructions,
          primaryartists,
          featuredartists,
          remixer_artists,
          producer_artists
        } = product_data;
        this.setState({
          meta_language,
          localized_language,
          release_date,
          upc_code,
          name,
          name_displayed,
          product_code,
          genre : Array.from(this.props.product.genre).filter(val => val.genre === genre).map(val => {return	{'label': val.genre,'value': val.id} }) 
					,
					sub_genre : Array.from(this.props.product.subgenre)
					.filter(val => val.sub_genre === sub_genre)
					.map(val => {return	{'label': val.sub_genre,'value': val.id} }) 
					,
					format,
					imprint,
          copy_line,
          product_highlights,
					special_instructions,
					primary_artists: primaryartists.map((artist) => {
						return { 'label': artist.Artist.name, 'value': artist.Artist.id.toString() }
					}),
					featured_artists :featuredartists.map((artist) => {
						return { 'label': artist.Artist.name, 'value': artist.Artist.id.toString() }
					}) ,
					remixers : remixer_artists.map((artist) => {
						return { 'label': artist.Artist.name, 'value': artist.Artist.id.toString() }
					}),
					producers: producer_artists.map((artist) => {
						return { 'label': artist.Artist.name, 'value': artist.Artist.id.toString() }
					})
          
				});
			this.state.genre  &&	this.setState({filteredSubGenre : Array.from(this.props.product.subgenre).filter(val => val.GenreId === this.state.genre[0].value) }); 
	
		 }    

	}


	handlePrimaryArtistsChange(selected_artist) {
		this.setState({ primary_artists: selected_artist })
	}

	handleFeaturedArtistsChange(selected_artist) {
		this.setState({ featured_artists: selected_artist })
	}

	handleProducerArtistsChange(selected_artist) {
		this.setState({ producers: selected_artist })
	}
	handleRemixerArtistsChange(selected_artist) {
		this.setState({ remixers: selected_artist })
	}
  
  
	handleValueChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	handleInputChange(input){
		if(input.length > 4){
				this.getArtists(input);
		}
	}

  getArtists(input){
		this.props.getArtists(this.props.auth.user.publisher , input)
	}

  handleMetaLangChange (newValue){
    this.setState({ meta_language : newValue });
  };
  handleLocalizedLangChange (newValue){
    this.setState({ localized_language : newValue });
  };

	handleGenreChange(genre){
		const {subgenre} = this.props.product;
		let filteredSubGenre = subgenre.filter(val => val.GenreId === Number(genre.value))
		this.setState({genre , filteredSubGenre , sub_genre : {}});
	}
	handleSubGenreChange(sub_genre){
		this.setState({sub_genre});
	}

	handleFormatChange(format){
		this.setState({format});
	}

  handleSubmit(e){
    e.preventDefault();
		const { meta_language,
			localized_language,
			release_date,
			upc_code,
			name,
			name_displayed,
			product_code,
			genre,
			sub_genre,
			format,
			imprint,
			copy_line,
			product_highlights,
			special_instructions,
			primary_artists,
			featured_artists,
			remixers,
			producers } = this.state;
			this.props.basicProduct({ meta_language: meta_language.label,
				localized_language : localized_language.label,
				release_date,
				upc_code,
				name,
				name_displayed,
				product_code,
				genre : genre.label,
				sub_genre : sub_genre.label,
				format : format.label,
				imprint,
				copy_line,
				product_highlights,
				special_instructions,
				primary_artists,
				featured_artists,
				remixers,
				producers,
				publisherId : this.props.auth.user.publisher.id,
				id : this.props.match.params.productId

			})
  }
	



  render() {
		const product_data = get(this.props.product, 'userAlbum.result', false);
		const product = get(this.props , 'product', false)
		const artist_data = get(this.props.artist, 'artists.artistPublisher.docs', [{ Artist : { value: '', id : ''}}]);

    const {  meta_language,
      localized_language,
      release_date,
      upc_code,
      name,
      name_displayed,
      product_code,
      genre,
      sub_genre,
      format,
      imprint,
      copy_line,
      product_highlights,
      special_instructions,
      primary_artists,
      featured_artists,
      remixers,
			producers,
			filteredSubGenre
		} = this.state;
			

		

    return ( !product.basicProductRequest ?
    product.fetchingProduct && !product_data ?
    <div className="content-loader" >
      <PulseLoader  loading={this.props.product.fetchingProduct && !product_data}  color={'#9b9b9b'} size={30} margin={'10px'}/>
      </div>
   
   : <div>
		{product.errorProductBasic && <Alert color="danger">An unknown error occured!</Alert>}
    <Form onSubmit={this.handleSubmit.bind(this)}>
		<span className="RequiredProductFieldNotification">
			<span>* indicates field required for submission of product</span>
		</span>
		<h2 className="DigitalProductForm-header">
			<span>Basics</span>
		</h2>
		<div className="DigitalProductForm-groups">

		    		<FormGroup>
		    			<Label for="upc-code">UPC * <TooltipCustom text="This is a 12 or 13 digit numeric code that identifies this product. If you do not have a UPC / EAN, one can be assigned for you." /></Label>
		    			<Input defaultValue={product_data.upc_code} disabled type="text" id="upc-code" name="upc-code" />
		    		</FormGroup>


		    			<FormGroup required>
		    			<Label for="meta-language">Meta Language * <TooltipCustom text="This is the language of the digital product's metadata" /></Label>
		    			
						{ !!meta_language	&&	 <Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
            options={data.languages}
            onChange={this.handleMetaLangChange.bind(this)}
            value={!meta_language.label ? data.languages.find(val => val.label === meta_language) : meta_language}
		      
		              id="meta-language"
					  name="meta-language"
						/> 			}
		    		</FormGroup>	


			<FormGroup required>
		    			<Label for="product-name">Product Name * <TooltipCustom text="The name of the product that will be sent to stores." /></Label>
		    			<Input type="text" value={name} onChange={this.handleValueChange} id="product-name" name="name" />
		    		</FormGroup>
		    		<FormGroup>
		    			<Label for="name-displayed">Product Name Displayed As</Label>
		    			<Input type="text" value={name} onChange={this.handleValueChange} readOnly className="DisplayedAs-input" id="name_displayed" name="name-displayed"  />
		    		</FormGroup>
		    		<FormGroup>
		    			<Label for="product-code">Product Code <TooltipCustom text="This is a unique code used to identify this product. Example: APPL123CD. Also known as Catalog Number. This is required as it's sent through the supply chain." /></Label>
		    			<Input type="text" value={product_code} onChange={this.handleValueChange} id="product-code" name="product_code" />
		    		</FormGroup>

		    		   			<FormGroup required>
		    			<Label for="primary-artist">Primary Artist(s) * <TooltipCustom text="The name of the artist(s) performing on the release, as you want it to appear in stores. Solo artists performing together must be entered as separate entries. Example: [Miles Davis] [John Coltrane]. If there are more than 3 primary artists, please type Various Artists." /></Label>
		    			
					<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={primary_artists}
						onChange={this.handlePrimaryArtistsChange.bind(this)}
						onInputChange={this.handleInputChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
							return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="primary-artist"
						/> 
			
		    		</FormGroup>	

		    		<FormGroup>
		    			<Label for="featured-artist">Featured Artist(s) <TooltipCustom text="Only enter artist(s) that will be featured on every track for this release. If the artist is not featured on every track, simply enter them as a featuring artist on the appropriate track at the track level." /></Label>
		    			
							<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={featured_artists}
						onChange={this.handleFeaturedArtistsChange.bind(this)}
						onInputChange={this.handleInputChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
							return { 'label': artist.Artist.name, 'value': artist.Artist.id.toString() }
						})}


					  name="featured_artist"
							/> 
			
		    		</FormGroup>
		    			<FormGroup>
		    			<Label for="remixers">Remixer(s) <TooltipCustom text="Only enter artist(s) that will be a remixer on every track for this release. If the artist is not a remixer on every track, simply enter them as a remixer on the appropriate track at the track level." /></Label>
		    			
		   	<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={remixers}
						onChange={this.handleRemixerArtistsChange.bind(this)}
						onInputChange={this.handleInputChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
							 return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="remixers"
					/> 
				</FormGroup>
		    			<FormGroup>
		    			<Label for="producers">Producer(s) <TooltipCustom text="Only enter artist(s) that will be a producer on every track for this release. If the artist is not a producer on every track, simply enter them as a producer on the appropriate track at the track level." /></Label>
		    			
							<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
						isMulti={true}
						value={producers}
						onChange={this.handleProducerArtistsChange.bind(this)}
						options={artist_data && artist_data.map((artist) => {
						  return { 'label': artist.Artist.name, 'value': artist.Artist.id }
						})}
					  name="producers"
						/> 
			
		    		</FormGroup>

		    		<h4 className="DigitalProductForm-header"><span>Release Details</span></h4>

            <FormGroup required>
		    			<Label for="release_date">Release Date * <TooltipCustom text="Enter a release date (e.g.: 12-12-2019)" /></Label>
		    			<Input type="date" value={release_date} onChange={this.handleValueChange} id="release_date" name="release_date" />
		    		</FormGroup>


		    		<FormGroup required>
		    			<Label for="genre">Genre * <TooltipCustom text="This is a recommendation only. Stores reserve the right to change the selected genre." /></Label>
		    			
		    	<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
						classNamePrefix="select"
						value={genre}
						onChange={this.handleGenreChange.bind(this)}
						options={product.genre ? Array.from(product.genre).map((genre) => {
							return { 'label': genre.genre, 'value': genre.id.toString()} }) : [] }

					  name="genre"
						/> 
			
		    		</FormGroup>

		    		<FormGroup required>
		    			<Label for="sub-genre">SubGenre * <TooltipCustom text="This is a recommendation only. Stores reserve the right to change the selected subgenre." /></Label>
		    			
		    		<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
						value={sub_genre}
						isDisabled={filteredSubGenre.length === 0}
						options={filteredSubGenre ? filteredSubGenre.map((genre) => {
							return { 'label': genre.sub_genre, 'value': genre.id.toString()} }) : [] }
						onChange={this.handleSubGenreChange.bind(this)}

					  name="sub-genre"
					/>
			
		    		</FormGroup>

		    		<FormGroup required>
		    			<Label for="format">Format * <TooltipCustom text="This indicates the product format that will be sent to stores." /></Label>
		    			
		    		<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
						options={data.formatOptions}
						name="format"
						value={!format.label ? data.formatOptions.find(val => val.label === format) : format}
						onChange={this.handleFormatChange.bind(this)}
					/>
			
		    		</FormGroup>

		    		<FormGroup required>
		    			<Label for="imprint">Imprint *</Label>
		    			<Input type="text" id="imprint" value={imprint} onChange={this.handleValueChange} name="imprint" />
		    		</FormGroup>

		    		<FormGroup required>
		    			<Label for="cline">(c)Line * <TooltipCustom text="This is the copyright information for track ordering, artwork and text of this product. Example: '1969 Apple Records'" /></Label>
		    			<Input type="text" value={copy_line} onChange={this.handleValueChange} id="cline" name="copy_line" />
		    		</FormGroup>

		    		<FormGroup>
		    			<Label for="product-highlights">Product Highlights <TooltipCustom text="This will be used in the Product Highlights section of Sales Sheets and other product materials." /></Label>
		    			<Input type="textarea" value={product_highlights} onChange={this.handleValueChange} id="product-highlights" name="product_highlights" />
		    		</FormGroup>


		    		<FormGroup>
		    			<Label for="special-instructions">Special Instructions <TooltipCustom text="Please include any special requests that this release requires like PDF booklet delivery or an Instant Grat." /></Label>
		    			<Input type="textarea" value={special_instructions} onChange={this.handleValueChange} id="special-instructions" name="special_instructions" />
		    		</FormGroup>

		    		<h4 className="DigitalProductForm-header"><span>Language Localizations <TooltipCustom text="Select localization language" /></span></h4>
		    					<FormGroup>
		    			<Label for="localized-language">Localized Language</Label>
		    			
						{	!!localized_language		 &&   		<Select
					  className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
					  classNamePrefix="select"
					  isSearchable={true}
            options={data.languages}
            onChange={this.handleLocalizedLangChange.bind(this)}
            value={!localized_language.label ? data.languages.find(val => val.label === localized_language) : localized_language}
					  cacheOptions={true}
		        id="localized-language"
					  name="localized-language"
						/> }
			
		    		</FormGroup>

		</div>

		    		<div className="DigitalProductFooter">
		    		<hr />
		    		<Button type="submit" id="digital-product-footer-next-button" className="DigitalProductFooter-button float-right btn btn-primary">SAVE <FaAngleRight /></Button>
		    		<Button type="button" className="DigitalProductFooter-back-button btn btn-default"><FaAngleLeft /> Overview</Button>
		    		</div>	
		</Form>
		</div> : <div className="content-loader" >
      <PulseLoader  loading={product.basicProductRequest}  color={'#9b9b9b'} size={30} margin={'10px'}/>
      </div>
   
    );		
  }
}


function mapDispatchToProps(dispatch) {
  return({
	getProduct: (id) => {dispatch(product.getProduct(id))}, 
	addArtist: (name, publisherId) => {dispatch(artist.addArtist(name, publisherId))},
	basicProduct: (data) => {dispatch(product.basicProduct(data))},
	getArtists: (publisher, name) => {dispatch(artist.getArtists(publisher, name))},
	getGenre: () => {dispatch(product.getGenre())},
	getSubGenre: () => {dispatch(product.getSubGenre())},
  })
}

function mapStateToProps(state) {
    return { 
    artist: state.artist,
    auth: state.auth,  
    ui:  state.ui,
    product : state.product
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductBasics);