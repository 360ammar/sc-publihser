import React from 'react';
import { connect } from 'react-redux';
import TooltipCustom from '../../../components/TooltipCustom';
import { FaInfoCircle } from 'react-icons/fa'
import PublishingTable from '../../../components/product/publishingTable'
import { product } from '../../../actions/product'
import { PulseLoader } from 'react-spinners'
import { get } from 'lodash'

class ProductPubObligations extends React.Component {



	constructor(props) {
		super(props);
		this.state = {
			tracks: [
				{
					'name': 'Track 1 Name'
				},
				{
					'name': 'Track Second Name'
				}
			],
			showApplyAll: undefined,
			pubOb : '',
			publisher: ''

		};
		this.setPublisher = this.setPublisher.bind(this)
	}


	componentDidMount() {
		this.props.getProduct(this.props.match.params.productId)
	}


	setPublisher(select, value) {
		this.setState({ publisher: value, pubOb : select })
	}


	render() {
		const { showApplyAll , pubOb , publisher } = this.state;
		const { fetchingProduct } = this.props
		const tracks = get(this.props.product , 'userAlbum.tracks' , false);
		return (
			fetchingProduct 

		?	<div className="content-loader" >
				 <PulseLoader  loading  color={'#9b9b9b'} size={30} margin={'10px'}/>
		 	</div>

		:	<div>
				<div className="PublishingObligation-content">
					<span className="RequiredProductFieldNotification"><span>* indicates field required for submission of product</span></span>
					<h2 className="PublishingObligation-header">Publishing Obligation</h2>
					<div>
						<div className="ImportantCopyrightNotice" >
							<span className="ImportantCopyrightNotice-label">
								important notice:
			</span>
							<span>
								You are ultimately responsible for obtaining and paying for any
													publishing licenses that may be needed for exploitation of your sound
													recordings. Your failure to do so would constitute not only a violations of
													applicable copyright and intellectual property laws in each country you make
													your recordings available, but also the rights of third parties, as well your
													contract with ShiaCloud. For More information on US copyright law, you may
													wish to seek legal advice; additional information is available at
                    <a className="ImportantCopyrightNotice-copyright" href="https://www.copyright.gov/" target="_blank">www.copyright.gov</a>.
								 For information with respect to the laws of other
								countries, you should consult analogous resources and consult with
                    attorneys qualified to render advice on such matters in those countries.</span>
						</div>

						<table className="PublishingObligationList">
							<thead>
								<tr className="PublishingObligationList-header">
									<th className="PublishingObligationList-trackNumber"></th>
									<th className="PublishingObligationList-trackName">Track Name</th>
									<th className="PublishingObligationList-obligationSelect">US Publishing Obligation * <TooltipCustom text="Indicate the publishing obligation for this track in the US. Note that if Option 1 applies (100% controlled/administered), publishing amounts will be paid directly through to you, and you are responsible for any and all publishing payments. If Option 3 applies, you are representing that no publishing licenses are required, and no deductions will apply."><FaInfoCircle /></TooltipCustom></th>
									<th className="PublishingObligationList-publishersTags">Publishers <TooltipCustom text="Publishers featured on this particular track"><FaInfoCircle /></TooltipCustom></th>
									<th className="PublishingObligationList-applyAll"></th></tr>
							</thead>

							<tbody>
								{tracks && tracks.map((val, i) =>	<PublishingTable index={i} track={val} publisher={publisher} pubOb={pubOb} setPublisher={this.setPublisher} /> )}

							</tbody>
						</table>




					</div>
				</div>

				<div className="DigitalProductFooter">
					<hr />
					<button type="button" id="digital-product-footer-next-button" className="DigitalProductFooter-button pull-right btn btn-primary">
						SAVE
				</button><button type="button" className="DigitalProductFooter-back-button btn btn-default">
						Overview
				</button>
				</div>

			</div>
		);
	}
}

function mapDispatchToProps(dispatch) {
	return({
		getProduct: (id) => {dispatch(product.getProduct(id))}, 
	})
}


function mapStateToProps(state) {
	return { 
		product : state.product
	};
}


export default connect(mapStateToProps,mapDispatchToProps)(ProductPubObligations);