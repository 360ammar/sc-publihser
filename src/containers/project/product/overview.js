import React from 'react';
import {DigitalProductOverviewSectionHeader, DigitalProductOverviewItem, DigitalProductOverviewTracks, DigitalProductOverviewPublishingObligation} from '../../../components/product/overview';
import { FaRegFileImage, FaRegFileAudio, FaRegFileAlt } from "react-icons/fa";
import {connect} from 'react-redux';
import {product} from '../../../actions/product'
import {PulseLoader} from 'react-spinners';
import {get} from 'lodash';
class ProductOverview extends React.Component {
	
	componentDidMount(){
		this.props.getProduct(this.props.match.params.productId)
	}
	componentDidUpdate(prevProps){
		this.props.match.params.productId !== prevProps.match.params.productId && this.props.getProduct(this.props.match.params.productId)
	}

  render() {
		console.log('thsi.props', this.props)
		const product_data = get(this.props.product, 'userAlbum.result', false);
		const tracks = get(this.props.product, 'userAlbum.tracks' , false);
		console.log(product_data);
    return (
			this.props.product.fetchingProduct && !product_data ?
			<div className="content-loader" >
				<PulseLoader  loading={this.props.product.fetchingProduct && !product_data}  color={'#9b9b9b'} size={30} margin={'10px'}/>
				</div>
			:
			<div>  
    	<section className="DigitalProductOverview-header"><h2 className="DigitalProductOverview-header-title">Product Overview</h2></section>
	    	<div className="DigitalProductOverview-section-wrapper">
	    		<DigitalProductOverviewSectionHeader linkRef={`/project/${this.props.match.params.id}/product/${this.props.match.params.productId}/basics`} title="Basics" isEditable={true} isDanger={false} />
	    		<section className="DigitalProductOverview-section">
	    			<DigitalProductOverviewItem item={{title : 'Upc' , value: product_data.upc_code , isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Meta Language' , value : product_data.meta_language  , isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Product Name' , value: product_data.name , isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Product Code' , value: product_data.product_code , isRequired:false , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Primary Artist(s)' ,  isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Featured Artist(s)' ,  isRequired:false , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Remixer(s)' ,  isRequired:false , fieldErrors: null ,extraOverlayClassNames: null}} />
					<DigitalProductOverviewItem item={{title : 'Producer(s)' ,  isRequired:false , fieldErrors: null ,extraOverlayClassNames: null}} />
	    			<DigitalProductOverviewItem item={{title : 'Genre' , value : product_data.genre ,  isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
					<DigitalProductOverviewItem item={{title : 'SubGenre' , value : product_data.sub_genre ,  isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
					<DigitalProductOverviewItem item={{title : 'Format' , value : product_data.format ,  isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
					<DigitalProductOverviewItem item={{title : 'Imprint' , value : product_data.imprint ,  isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
					<DigitalProductOverviewItem item={{title : '(C) Line' , value : product_data.copy_line , isRequired:true , fieldErrors: null ,extraOverlayClassNames: null}} />
					<DigitalProductOverviewItem item={{title : 'Special Instructions' , value : product_data.special_instructions ,  isRequired:false , fieldErrors: null ,extraOverlayClassNames: null}} />
					
		    		</section>
	    	</div>
	    	<div className="DigitalProductOverview-section-wrapper">
	    		<DigitalProductOverviewSectionHeader linkRef={`/project/${this.props.match.params.id}/product/${this.props.match.params.productId}/artwork`} title="Artwork" isEditable={true} isDanger={false} />
	    		<section className="DigitalProductOverview-section">


	    		{ !!product_data.artwork220 ?  
							<img className="mx-auto" src={`http://api.shiacloud.net/${product_data.artwork220}`} />
					:	<div className="DigitalProductOverviewArtwork-not-found">
	    			<div className="DigitalProductOverviewArtwork-empty-icon">
	    				<FaRegFileImage className="Icons-artwork-empty" />
	    			</div>
	    				You need to add artwork
	    		</div>}


	    		</section>
	    	</div>
	    	<div className="DigitalProductOverview-section-wrapper">
	    		<DigitalProductOverviewSectionHeader linkRef={`/project/${this.props.match.params.id}/product/${this.props.match.params.productId}/tracks`} title="Tracks" isEditable={true} isDanger={false} />
	    		<section className="DigitalProductOverview-section DigitalProductOverview-section-no-paddings">
					{tracks ?	<DigitalProductOverviewTracks tracks={tracks} />
	    			:	<div className="DigitalProductOverviewTracks-not-found">
	    			<div className="DigitalProductOverviewTracks-empty-icon">
	    				<FaRegFileAudio className="Icons-tracks-empty" />
					</div>You need to add tracks</div> }
	    		</section>
	    	</div>
	    	<div className="DigitalProductOverview-section-wrapper">
	    		<DigitalProductOverviewSectionHeader linkRef={`/project/${this.props.match.params.id}/product/${this.props.match.params.productId}/publishing-obligations`} title="Publishing Obligation" isEditable={true} isDanger={false} />
	    		<section className="DigitalProductOverview-section DigitalProductOverview-section-no-paddings">
	    			<DigitalProductOverviewPublishingObligation />

	    			<div className="DigitalProductOverviewTracks-not-found">
	    				<div className="DigitalProductOverviewTracks-empty-icon">
	    				<FaRegFileAlt className="Icons-tracks-empty" />
	    				</div><span>Publishing Obligations Needed</span>
	    			</div>
	    		</section>
	    	</div>

    	</div>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return({
  getProduct: (id) => {dispatch(product.getProduct(id))}, 
  })
}

function mapStateToProps(state){
    return {
		artist: state.artist,
		auth: state.auth,
        ui: state.ui,
		project: state.project,
		product: state.product
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductOverview);