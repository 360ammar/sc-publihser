import React from 'react';
import {connect} from 'react-redux';
import {Row, Col,Nav, NavItem, Button} from 'reactstrap';
import {NavLink, Route, Link } from 'react-router-dom';

export class ProductSidebar extends React.Component {

    componentDidMount(){
        this.props.setActiveProduct.bind(null,this.props.product.id);
    }
   
    render(){
        const {active, product, projectid, setActiveProduct} = this.props;
    return (
        <React.Fragment>
      <NavItem className="ProjectNavigation-product-wrapper">
      {!active ?  <NavLink onClick={setActiveProduct.bind(this, product.id)} className={'Opm-navigation-link'} activeClassName={'Opm-navigation-selected'} to={`/project/${projectid}/product/${product.id}/overview`}>
          <div className="Opm-navigation-container">
            <h4 className="Opm-navigation-format">{product.format}</h4>
            <span className={`icon-validation icon-progress`}></span> 
            <span className="Opm-navigation-status-heading">IN PROGRESS</span>
          </div>
        </NavLink>
        

    : (
        <div className="Opm-digital-product-navigation">
            <div className="Opm-digital-product-navigation-container" >
            <div class="Opm-digital-product-navigation-text-container">
            <h4 class="Opm-navigation-format">{product.format}</h4>
            </div>
            <span className={`icon-validation icon-progress`}></span> 
            <span className="Opm-navigation-status-heading">IN PROGRESS</span>
           
            
            
        <div className="Opm-digital-product-navigation-steps">
        <div className="Opm-digital-product-navigation-step">
            <NavLink className="Opm-digital-product-navigation-link"  activeClassName="Opm-digital-product-navigation-selected" to={`/project/${projectid}/product/${product.id}/overview`}>
                <div className="Opm-digital-product-navigation-link-text">Overview</div>
            </NavLink>
        </div>
        <div className="Opm-digital-product-navigation-step">
            <NavLink className="Opm-digital-product-navigation-link" activeClassName="Opm-digital-product-navigation-selected" to={`/project/${projectid}/product/${product.id}/basics`}>
                <div className="Opm-digital-product-navigation-link-text">Basics</div>
            </NavLink>
        </div>
        <div className="Opm-digital-product-navigation-step">
            <NavLink className="Opm-digital-product-navigation-link" activeClassName="Opm-digital-product-navigation-selected" to={`/project/${projectid}/product/${product.id}/artwork`}>
                <div className="Opm-digital-product-navigation-link-text">Artwork</div>
            </NavLink>
        </div>
        <div className="Opm-digital-product-navigation-step">
            <NavLink className="Opm-digital-product-navigation-link" activeClassName="Opm-digital-product-navigation-selected" to={`/project/${projectid}/product/${product.id}/tracks`}>
                <div className="Opm-digital-product-navigation-link-text">Tracks</div>
            </NavLink>
        </div>
        <div className="Opm-digital-product-navigation-step">
            <NavLink className="Opm-digital-product-navigation-link" activeClassName="Opm-digital-product-navigation-selected" to={`/project/${projectid}/product/${product.id}/marketing`}>
                <div className="Opm-digital-product-navigation-link-text">Marketing</div>
            </NavLink>
        </div>
        
        
        <div className="Opm-digital-product-navigation-step">
            <NavLink className="Opm-digital-product-navigation-link" activeClassName="Opm-digital-product-navigation-selected" to={`/project/${projectid}/product/${product.id}/publishing-obligations`}>
                <div className="Opm-digital-product-navigation-link-text">Publishing Obligation</div>
            </NavLink>
        </div>
        <div className="Opm-digital-product-navigation-validate-container Opm-digital-product-navigation-validate-button">
            <Button className=" btn btn-default">Validate Product</Button>
        </div>
        </div>
        </div>
    </div> )}
    </NavItem>
    </React.Fragment>
      
    );		
    }
}