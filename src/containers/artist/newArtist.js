import React from 'react';
import {connect} from 'react-redux';
import {Row, Col, NavItem} from 'reactstrap';
import {NavLink, Route} from 'react-router-dom'
import ArtistInfo from './artistInfo';
import ArtistImage from './artistImage';
import {artist} from '../../actions/artist';
import {PulseLoader} from 'react-spinners'

class NewArtist extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            artistId : (isNaN(props.match.params.id) ? null : props.match.params.id ) 
        };
    }    

    componentDidMount(){
        this.state.artistId && this.props.getArtist(this.state.artistId);
    }

    render(){
        const {props} = this.props;
        return (
            <div className="ProjectManager-wrapper">
            <Row >
                <Col>
                <header class="ProjectManager-header">
                <article>
                    <h1 class="ProjectHeader-name"></h1>
                </article></header>
                </Col>
                </Row>    
                <Row className="ProjectManager-main">
                <Col md="3" className="px-0">
                <aside className="ProjectManager-aside">
                <nav>
                <NavItem className="ProjectNavigation-product-wrapper">
              <NavLink className="Opm-navigation-link" activeClassName="Opm-navigation-selected" to={`/artist/${isNaN(this.props.match.params.id) ? 'create' : this.props.match.params.id }/info`}>
              <div className="Opm-navigation-container"><h4 className="Opm-navigation-format">General Info</h4></div>
              </NavLink>
            </NavItem>
           { !isNaN(this.props.match.params.id) && <NavItem className="ProjectNavigation-product-wrapper">
              <NavLink className="Opm-navigation-link" activeClassName="Opm-navigation-selected" to={`/artist/${this.props.match.params.id}/image`}>
              <div className="Opm-navigation-container"><h4 className="Opm-navigation-format">Artist Image</h4></div>
              </NavLink>
            </NavItem> }

                </nav>
                </aside>
                </Col>
                <Col md="9" className="px-0">
                {this.props.artist.fetchingArtist && <div className="ProjectManager-content content-loader" >
				<PulseLoader  loading  color={'#9b9b9b'} size={30} margin={'10px'}/>
                </div> }
                <article className={`ProjectManager-content ${this.props.artist.fetchingArtist && 'invisible'}`}>
                
        <Route exact path={`/artist/${isNaN(this.props.match.params.id) ? 'create' : this.props.match.params.id }/info`} render={(props) => <ArtistInfo {...props} />} />
                <Route  path={'/artist/:id/image'} render={(props) => <ArtistImage {...props} />} />
       
                </article>
                </Col>
                </Row>

           
            
            </div>
        );
    }


}

function mapDispatchToProps(dispatch) {
    return({
		getArtist: (id) => {dispatch(artist.getArtist(id))}
    })
}

function mapStateToProps(state){
    return {
        artist: state.artist
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewArtist);