import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input, FormFeedback, FormText, Alert } from 'reactstrap';
import Select, { createFilter }  from 'react-select';
import data from '../../data'
import { artist } from '../../actions/artist';
import { project } from '../../actions/project';
import { connect } from 'react-redux';
import {get} from 'lodash';
import { PulseLoader } from 'react-spinners';

const stringify = option => option.label;
const filterOption = createFilter({ ignoreCase: false, stringify });


class ArtistInfo extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			id : null,
			name: '',
			location: undefined,
			dob: '',
            biography: '',
            year_from : undefined,
            year_to: undefined,
            active_now : false, 
			errors: { 
				dob: false,
				location: false,
				name: false,
			}
		};


		this.onSubmitArtist = this.onSubmitArtist.bind(this)
		
		this.handleValueChange = this.handleValueChange.bind(this);
		this.handleLocationChange = this.handleLocationChange.bind(this);

	}

	componentDidMount(){
		if(get(this.props.artist, 'artist.artist' , false)){
			const { location, name, dob, biography, year_from, year_to, active_now } = this.props.artist.artist.artist;
			this.setState({ location : data.countries.filter((country)=> country.label === location)[0], name, dob : dob && dob.substring(0, 10), biography, year_from : year_from && year_from.substring(0, 10), year_to: year_to && year_to.substring(0, 10), active_now });
		}
	}

	componentDidUpdate(prevProps){
		if( prevProps.artist.creatingArtist === true && get(this.props.artist, 'artistCreated.respone' , false)){
			this.props.history.push(`/artist/${this.props.artist.artistCreated.respone.id}/image`)
		}
		if(prevProps.artist !== this.props.artist && get(this.props.artist, 'artist.artist' , false)){
			const { location, name, dob, biography, year_from, year_to, active_now } = this.props.artist.artist.artist;
			this.setState({ location : data.countries.filter((country)=> country.label === location)[0], name, dob : dob && dob.substring(0, 10), biography, year_from : year_from && year_from.substring(0, 10), year_to: year_to && year_to.substring(0, 10), active_now });
		}
		
	}

	handleValueChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    	this.setState({ [name]: value });
	}

	

	handleLocationChange(selected_location) {
		this.setState({ location: selected_location })
	}

	onSubmitArtist(event) {
		event.preventDefault();
		const { location, name, dob, biography, year_from, year_to, active_now } = this.state;
		
		let errors = { 
			dob: false,
			location: false,
			name: false,
		}
		
		if (location === undefined) {
			errors.location = true;
		}
		if (name === '') {
			errors.name = true;
		}
		if (dob === '') {
			errors.dob = true;
		}
		this.setState({ errors: errors });
		let data = { location: location && location.label , year_from, year_to, active_now, name, dob, biography, publisherId: this.props.auth.user.publisher.id };
		if(!(errors.dob || errors.location || errors.name))
			this.props.addArtist(data);
	}
	
	render() {
		console.log(this.select)
		console.log(data.countries)
		const { location, name, dob, biography, year_from, year_to, active_now } = this.state;
		return (
			<React.Fragment>
				{ !(get(this.props.artist, 'artists.artistPublisher', false))  ?
			 <div className="ProjectLandingPage">
				<Col className="ProjectLandingPage-selectors">
					<Col>
						<h1 className="ProjectLandingPage-header">Artist</h1>
						<Form onSubmit={(e) => this.onSubmitArtist(e)} className="ProjectForm">
							<FormGroup className={`${this.state.errors.name && 'FormField-group-has-error' }`} required>
								<Label className="FormField-label" for="name">Artist Name *</Label>
								<Input className="FormField-form-control" placeholder="Artist Name" value={name} name="name" onChange={this.handleValueChange} invalid={this.state.errors.name} />
								<FormFeedback>This information is required</FormFeedback>
							</FormGroup>
							<FormGroup className={`${this.state.errors.dob && 'FormField-group-has-error' }`} required >
								<Label className="FormField-label" for="artist-dob">Date of Birth *</Label>
								<Input  className="FormField-form-control" onChange={this.handleValueChange} type="date" value={dob} name="dob" invalid={this.state.errors.dob} />
								<FormFeedback>This information is required</FormFeedback>
							</FormGroup>
							<FormGroup required className={`ProjectArtist ${this.state.errors.location && 'FormField-group-has-error'}`}>
								<Label className="FormField-label" for="location">Artist Location *</Label>
								<div>
									<span className="SelectInputWithAddNew-container">
										<div><Select
											ref ={c => this.select = c}
											className="Select SelectInput-select SelectInputWithAddNew-dropdown Select--single is-clearable is-searchable"
											classNamePrefix="select"
											isSearchable={true}
											name="location"
											value={location}
											matchPos="any"
											matchProp="any"
											options={data.countries}
											onChange={this.handleLocationChange}
										/>
										</div>
										{this.state.errors.location == true && <div className="invalid-feedback">This information is required</div>}
										

										
									</span>
								</div>

							</FormGroup>
                            <FormGroup>
								<Label className="FormField-label" for="artist-from">Artist Active From</Label>
								<Input  className="FormField-form-control" onChange={this.handleValueChange} type="date" value={year_from} name="year_from" />
							</FormGroup>
                            <FormGroup>
								<Label className="FormField-label" for="artist-to">Artist Active To</Label>
								<Input  className="FormField-form-control" onChange={this.handleValueChange} type="date" disabled={active_now} value={year_to} name="year_to" />
								
								<Input id="active"  className="FormField-form-control ml-2" checked={active_now} onChange={this.handleValueChange} type="checkbox" value={active_now} name="active_now"  />
								<Label for="active" className="FormField-label ml-5">Artist is still active</Label>
							</FormGroup>
                       
							<FormGroup >
								<Label className="FormField-label" for="biography">Biography </Label>
								<Input  className="FormField-form-control" type="textarea" value={biography} onChange={this.handleValueChange} name="biography" id="biography" />
							</FormGroup>
							<hr />
							<Button className="ProjectForm-save-button btn btn-primary">{this.props.artist.creatingArtist ? <PulseLoader  loading  color={'#fff'} />  : 'Save Changes'}</Button>

						</Form>
					</Col>
				</Col>
									</div> : <div className="content-loader" >
				<PulseLoader  loading  color={'#9b9b9b'} size={30} margin={'10px'}/>
				</div> }
						</React.Fragment>			);
	}
}


function mapStateToProps(state) {
	return {
		artist: state.artist,
		artists: state.artists,
		auth: state.auth,
		ui: state.ui,
		
	}
}

function mapDispatchToProps(dispatch) {
    return({
		addArtist: (name, publisherId) => {dispatch(artist.addArtist(name, publisherId))},
		getArtist: (publisher, name) => {dispatch(artist.getArtist(publisher, name))},
		getArtists: (publisher, name) => {dispatch(artist.getArtists(publisher, name))},
		addProject: (data) => {dispatch(project.addProject(data))}, 
    })
}


export default connect(mapStateToProps, mapDispatchToProps)(ArtistInfo);