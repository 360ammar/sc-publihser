import React from 'react';
import {connect} from 'react-redux';
import {Alert} from 'reactstrap'
import Dropzone from 'react-dropzone';
import {FaRegFileImage} from 'react-icons/fa'
import {artist} from '../../actions/artist'
import {get} from 'lodash'
import { PulseLoader } from 'react-spinners';
const dropzoneRef = React.createRef();
class ProductArtwork extends React.Component {

	constructor(props){
		super(props);
    this.state ={accepted: null, rejected : [], errFileType:false, imgSrc: null };
    
    this.SubmitImage = this.SubmitImage.bind(this);
  }
  
  componentDidMount(){
    if(get(this.props.artist, 'artist.artist' , false)){
			const { image } = this.props.artist.artist.artist;
			this.setState({ imgSrc : image ? 'http://api.shiacloud.net/' + image : null });
		}
  }

  componentDidUpdate(prevProps){
    if(prevProps.artist !== this.props.artist && !(this.state.accepted) && get(this.props.artist, 'artist.artist' , false)){
      const { image } = this.props.artist.artist.artist;
			this.setState({ imgSrc : image ? 'http://api.shiacloud.net/' + image : null });
    }
  }


  SubmitImage(e){
    e.preventDefault();

    let data = new FormData();
    data.append('avatar', this.state.accepted[0] ,'image.jpg');
    data.append('id', this.props.match.params.id);

    this.props.addArtistImage(data);
  }

	 onDrop(accepted, rejected) {
        const self = this;
        this.setState({
            accepted, rejected
        });

    

	        if (accepted[0] && accepted) {
			  var img = new Image();

			  // img.onload = function () {
			  //   console.log(this.width + " " + this.height);
			  //   self.setState({imgWidth: this.width,
			  //   				imgHeight: this.height
			  //   });

			  // };

			  var reader = new FileReader();
			    reader.onloadend = function (ended) {
          img.src = ended.target.result;
          self.setState({imgSrc: ended.target.result
              });
			  }
			reader.readAsDataURL(accepted[0]);
			}


        if(rejected.length !== 0)  this.setState({errFileType: true});
    }

  render() {
    console.log(this.props.match.params.id);
    return (
		<div className="ProductArtwork">
  			<h2 className="ProductArtwork-heading">Artist Image</h2>
      {this.props.artist.uploadingSuccess && <Alert color='success'>Upload success!</Alert>}
      
      {this.props.artist.errorImageUpload && <Alert color='danger'>There was an error occured uploading image. Please try again.</Alert>}
  			<section className="ArtworkUpload ArtworkUpload--error">
    		

    		   <Dropzone className="ArtworkUpload-dropzone" aria-disabled="false"
            accept="image/*"
            ref={dropzoneRef}
            onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
          >
          	<button type="button" className="btn btn-default">
    			    Upload artwork
		        </button>
      	
         
        
          <FaRegFileImage className="Icons-artist-image" />
          </Dropzone>
  { !!(this.state.imgSrc || this.state.accepted)  && <div><article className="ArtworkUpload-success" 
        style={{ backgroundImage : "url("+ this.state.imgSrc  +")" }}>
        <PulseLoader loading={this.props.artist.uploadingImage} size={30} margin={'15px'} color={'#ececec'} />
        </article></div> }
       {!!(this.state.imgSrc) && <button type="button" className="ArtworkUpload-replace-button btn btn-danger" onClick={() => dropzoneRef.current.open()}>
    			    Replace Artwork
    </button> }
        </section>
       
        <div className="DigitalProductFooter">
        <hr/>
        <button type="button" onClick={this.SubmitImage} id="digital-product-footer-next-button" className="DigitalProductFooter-button pull-right btn btn-primary">
        > SAVE</button>
        <button type="button" className="DigitalProductFooter-back-button btn btn-default">

  Overview</button>

  </div></div>
    );		
  }
}

function mapStateToProps(state) {
	return {
		artist: state.artist,
		auth: state.auth,
	}
}

function mapDispatchToProps(dispatch) {
    return({
		addArtistImage: (data) => {dispatch(artist.addArtistImage(data))}
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductArtwork);