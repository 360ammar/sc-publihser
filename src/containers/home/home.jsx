import React from 'react';
import { connect } from 'react-redux';
import styles from './home.css';
import {Col , Row, Button} from 'reactstrap';
import {Link } from 'react-router-dom';
import AsyncSelect from 'react-select/lib/Async';
import { project } from '../../actions/project';
import { product } from '../../actions/product';
import {get} from 'lodash'
import { PulseLoader } from 'react-spinners';

class HomeContainer extends React.Component {

	constructor(props) {
	    super(props);


	  }	
  
  componentDidMount() {
   		//this.props.dispatch(project.getProjects())
	this.props.getProjects();
	this.props.getProducts();
  }

  render() {
	  
		const { projectData, productData } = this.props;
		const projects = get(projectData , 'projects.projectPublisher.docs', []);
		const products = get(productData , 'products.albumPublisher.docs', []);
		console.log(project)
	  
    return (

  <Row>
	  <Col md="8" xs="12">
		  <Row className={styles.modules}>
		    <div className={styles.heading}><h3>Search your Catalog</h3></div>
			<div className={styles.searchform}>
				<div className={styles.searchprojects}>
				<AsyncSelect
		          
		        	/>
	        	</div>
	        	<div className={styles.buttonform}>
	        	<Link to="/project/create-new"><Button color="primary" size="lg">+ New Project</Button></Link>
	        	</div>
	        </div>
		  </Row>
		  <Row className={styles.modules}>
		  	    <h3>Recent Projects</h3>
		    	<table className="ClickableTable table">
			    	<thead>
				    	<tr><th>PROJECT NAME</th>
				    	<th>PROJECT CODE</th>
				    	<th>DATE CREATED</th>
				    	</tr>
				    </thead>
			    	<tbody>
			    		{
			    	projects.length > 0 ?	projects.map(function(p, index){
	                    		return <tr className="ClickableTable-row" key={ index }>
							    	<td><a href={"/project/" + p.id} className="RecentProjectsList-project-name">{p.project.project_name}</a></td>
							    	<td>{p.project.code}</td>
							    	<td>{(p.createdAt).substring(0,10)}</td>
						    	</tr>;
	                  		}) : <PulseLoader loading />
			    		}
			    	</tbody>
		    	</table>
		    	<div className="RecentProjectsList-button-row">
			    	<a href="/projects">
				    	<button type="button" className="RecentProjectsList-button-link btn btn-link">
				    		View All Projects
					    	
				    	</button>
			    	</a>
		    	</div>
		  </Row>
		  <Row className={styles.modules}>
		  	    <h3>Products In Progress</h3>
		    	<table  className="ClickableTable table">
			    	<thead>
			    		<tr>
					    	<th className="header">&nbsp;</th>
	                        <th className="ri releaseType_th text-nowrap header">Product Name</th>
	                        <th className="header">Artists</th>
	                        <th className="rd text-nowrap header">Release date</th>
	                   	</tr>
				    </thead>
			    	<tbody>
			    		{
			    			products.length > 0 ?		products.map(function(p, index){
	                    	if(p.Album) return <tr className="ClickableTable-row" key={ index }>
							    	<td><a href={`/project/${p.Album.project_albums.length > 0 ? p.Album.project_albums[0].projectId : '1'}/product/` + p.id + "/overview"} className="RecentProjectsList-project-name">
							    	<img width="40" height="40" src={ !!(p.Album.artwork38) ? `https://api.shiacloud.net/${p.Album.artwork38}` : "assets/images/products/1.jpeg"} />
							    	</a></td>
							    	<td><a href={`/project/${p.Album.project_albums.length > 0 ? p.Album.project_albums[0].projectId : '1'}/product/` + p.id + "/overview"} className="RecentProjectsList-project-name">
							    	{p.Album.name}
							    	</a></td>
							    	<td><span className="home-artists">{p.Album.artist_albums.map(artist => {
										if(artist.Artist)	return <span className="home-artist-name">{artist.Artist.name}</span>
										})}</span></td>
							    	<td>{p.Album.release_date || '0000-00-00'}</td>
						    	</tr>;
	                  		}) : <PulseLoader loading />
		    			}
			    	</tbody>
		    	</table>
		    	<div className="RecentProjectsList-button-row panel-footer">
	                <a className="float-right view-more" href="\products">
	                    <span className="font-weight">
	                    	View more 
	                    </span>
	                </a>
	                <div className="clear"></div>
	            </div>
		  </Row>
	  </Col>
	  <Col className={styles.contentRight}>
	  		 <Row className={styles.modules}>
		  	    <h3>Support</h3>
		    <p className={styles.description}> </p>
		  </Row>
	  </Col>
  </Row>
    );
  }
}

function mapStateToProps(state) { 
	return {
		projectData: state.project,
		productData: state.product,
	};
}

function mapDispatchToProps(dispatch) {
    return({
		getProjects: () => {dispatch(project.getProjects())},
		getProducts: () => {dispatch(product.getProducts())}
    })
}

const Home = connect(mapStateToProps, mapDispatchToProps)(HomeContainer);

export default Home;