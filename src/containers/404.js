import React from 'react'
import { Col, Row } from 'reactstrap'

export const NotFound = () => {

    return (
        <Row>
            <Col>
            <div id="notfound">
                <div class="notfound">
                    <div class="notfound-404">
                        <h1>Oops!</h1>
                        <h2>An unknown error occured!</h2>
                    </div>
                    <a href="/">Go TO Homepage</a>
                </div>
            </div>

            </Col>
        </Row>
    )

}