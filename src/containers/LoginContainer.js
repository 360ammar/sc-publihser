import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, Container, FormFeedback } from 'reactstrap';
import { auth } from '../actions/auth';
import { LoadingIndicator } from '../components/Loading';
import {PulseLoader} from 'react-spinners';
class LoginContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      errors: {
        email: false,
        password: false
      }

    };

    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    document.title = 'ShiaCloud | Login'
  }
  componentDidMount() {
    if (this.props.auth.authenticated) {
      this.props.dispatch(auth.logout());
    }
  }

  handleValidSubmit(e) {
    e.preventDefault();
    const { email, password } = this.state;
    const { dispatch } = this.props;
    if (email !== '' && password !== '') {
      dispatch(auth.login(email, password));
    }
    else {
      
      let errors = Object.assign({}, this.state.errors);
      if (email === '') errors.email = true;
      if (password === '') errors.password = true;
      this.setState({
        errors
      })

    }
  }
  onChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  render() {

    const { email, password } = this.state;

    return (
      <Container className="loginWrapper" >
        <div id="alw-login">
          <section className="login-section">
            <div id="header" className="text-center"><h1 id="logoLink"><Link to="/" title="ShiaCloud"><img src="https://www.shiacloud.net/images/logo.png" id="login-logo" /></Link></h1><div className="clear"></div></div>
              <Form onSubmit={this.handleValidSubmit}>
                <FormGroup required className={`${this.state.errors.email && 'FormField-group-has-error'}`}>
                  <Label for="email" >Email</Label>
                  <Input type="email" value={email} disabled={this.props.auth.loggingIn} onChange={this.onChange} name="email" id="email" invalid={this.state.errors.email} />
                  <FormFeedback>This information is required</FormFeedback>
                </FormGroup>
                <FormGroup className={`${this.state.errors.password && 'FormField-group-has-error'}`} required>
                  <Label for="password" >Password</Label>
                  <Input disabled={this.props.auth.loggingIn} type="password" value={password} onChange={this.onChange} name="password" id="password" invalid={this.state.errors.password} />
                  <FormFeedback>This information is required</FormFeedback>
                </FormGroup>
                <p className="text-right"><Link to="#">Forgot your Email or Password?</Link></p>
                <Button color="primary" className="pull-right login-button">{!this.props.auth.loggingIn ? 'Log In' :  <PulseLoader color={'#fff'} loading={true} margin={'3px'} /> }</Button>
              </Form>
            
          </section>
        </div>


      </Container>
    );
  }
}
function mapStateToProps(state) {
  return {
    auth: state.auth,
    alert: state.alert
  };
}


export default connect(mapStateToProps)(LoginContainer);
