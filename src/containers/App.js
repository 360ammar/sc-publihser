import React, { Component } from 'react';
import { connect } from 'react-redux';
import History from '../helpers/history';
import {auth} from '../actions/auth';
import { Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container } from 'reactstrap';

  import history from '../helpers/history';
import project from './project';
class App extends Component {

  constructor(props){
  	super(props);
    this.toggle = this.toggle.bind(this);
    this.validateUser = this.validateUser.bind(this);
    this.state = {
      isOpen: false,
      validate : false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  validateUser(){
    const user = localStorage.getItem('user');
    if(user && (jwt_decode(JSON.parse(user).token).exp > parseInt(Date.now().valueOf() / 1000)) && !!this.props.auth.authenticated ){
      this.setState({validate : true});
    }
    else{ 
      this.setState({validate : false});
    }
  }
  componentWillMount(){
    const user = localStorage.getItem('user');
    let decodedToken = user ? jwt_decode(JSON.parse(user).token) : false;
    if(user && (decodedToken.exp > parseInt(Date.now().valueOf() / 1000)) && !!this.props.auth.authenticated ){
      this.setState({validate : true});
    }
    else{ 
      this.setState({validate : false});
    }
  }


  render() {


    return ( (this.state.validate) ?
    <div>	
    <Navbar color="dark" dark expand="md">
        <Container>
          <NavbarBrand><NavItem>
                <NavLink to="/">ShiaCloud</NavLink>
              </NavItem></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
           <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink to="/catalog">Catalog</NavLink>
              </NavItem>
              </Nav>
            <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>
                    Option 2
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem onClick = {() => {
                    this.props.dispatch(auth.logout())
                    } }>
                   Logout 
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
          </Container>
        </Navbar>
        <Container>

      {this.props.children}
      
    	</Container></div> 
    	 : History.location.pathname !== '/login' && <Redirect push to={'/login'} /> 
    );
  }
}

function mapStateToProps(state) {
  return {
    auth : state.auth,
    alert: state.alert
  };
}


export default connect(mapStateToProps)(App);
