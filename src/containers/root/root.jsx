import { Route } from 'react-router-dom';
import React from 'react';
import { Features } from '../features';
import { NavLink } from '../../components/nav-link';
import { Header } from '../../components/header';
import { Home } from '../home';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import styles from './root.css';

export const Root = () => (
     
  <div>
    <AppBar position="static">
        <Toolbar>
          <IconButton color="inherit" aria-label="Menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit">
            News
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    <Header>
      <NavLink href={'/'}>Home</NavLink>
      <NavLink href={'/features'}>Libraries/Tools</NavLink>
    </Header>

    <div className={styles.content}>
      <Route path={'/'} exact component={Home} />
      <Route path={'/features'} component={Features} />
    </div>
  </div>
);

export default Root;
