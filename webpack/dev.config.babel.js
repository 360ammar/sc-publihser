import { HotModuleReplacementPlugin } from 'webpack';
import { buildConfig, APP_PATH } from './config';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import StyleLintPlugin from 'stylelint-webpack-plugin';
import merge from 'webpack-merge';
import path from 'path';

export default (env, argv) => merge(buildConfig(env, argv), {
  entry: [
    'react-hot-loader/patch',
  ],
  output: {
    path: path.resolve(APP_PATH, './assets'),
    filename: 'main.js'
  },

  devtool: 'cheap-module-eval-source-map',

  module: {
    rules: [{
      test: /\.css$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader',
        query: {
          modules: true,
          localIdentName: '[path][local]__[name]__[hash:base64:5]'
        }
      }, {
        loader: 'postcss-loader'
      }],
      
    },
      {
    
            test: /\.sass$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        }    ]
  },

  plugins: [
    new HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      inject: false,
      template: path.join(APP_PATH, 'template.html'),
      favicon: path.join(APP_PATH, 'assets', 'images', 'favicon.ico'),
    }),
    new StyleLintPlugin({
      configFile: '.stylelintrc',
      context: APP_PATH,
      files: '**/*.css'
    }),
  ],

  performance: {
    hints: false,
  },

  optimization: {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
  },

  devServer: {
    historyApiFallback: {disableDotRule: true},
    contentBase: APP_PATH,
    openPage: '',
    inline: true,
    open: true,
    port: 62000,
    hot: true,

    stats: 'errors-only',
  },
});
